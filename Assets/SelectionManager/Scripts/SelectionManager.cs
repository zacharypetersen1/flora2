﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class SelectionManager : MonoBehaviour
{

    public bool canMakeSelections = true;
    public GameObject hoverProjector;
    public GameObject selectionProjector;

    public enum mouseButtons { selectButton, deselectButton, addButton };

    public static bool selectMode = false;

    //custom inspector values
    public GameObject unitsContainer;

    public bool selectByName = false;
    public string selectedName;

    public KeyCode keyMultiselect;
    public KeyCode keyDeselect;

    public KeyCode keySetControlGroup;
    public KeyCode keyAddToControlGroup;

    List<SelectableObject> selectedGameObjects = new List<SelectableObject>();
    List<SelectableObject> hoveredGameObjects = new List<SelectableObject>();
    List<GameObject> disabledObjects = new List<GameObject>();
    List<ControlGroupUnit> controlGroupUnits = new List<ControlGroupUnit>();

    bool isSelecting = false;
    Vector3 mousePosition;

    float lastClickTime;
    float doubleClickDelay = 0.2f;
    bool hasDoubleClicked = false;

    struct ControlGroupUnit
    {
        public SelectableObject selectableObject;
        public int controlGroup;

        public ControlGroupUnit(SelectableObject selectableObject, int controlGroup)
        {
            this.selectableObject = selectableObject;
            this.controlGroup = controlGroup;
        }
    }

    struct SelectableObject
    {
        public GameObject gameObject;
        public GameObject projectorGameObject;
        public Projector projector;
        public SelectableUnit selectable;
        public bool isActive;
    }

    // Use this for initialization
    void Start()
    {
        if (!Camera.main)
        {
            Debug.Log("The SelectionManager requires a camera with the tag 'MainCamera'");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Camera.main)
            return;

        if (unitsContainer)
        {

            // Remove selected objects that no longer exist
            selectedGameObjects = selectedGameObjects.Where(i => i.gameObject != null).ToList();
            disabledObjects = disabledObjects.Where(i => i != null).ToList();

            setProjectors();

            // Remove any disabled objects
            selectedGameObjects = selectedGameObjects.Where(i => !disabledObjects.Any(j => j == i.gameObject)).ToList();
            hoveredGameObjects = hoveredGameObjects.Where(i => !disabledObjects.Any(j => j == i.gameObject)).ToList();

            if (selectMode)
            {
                if (canMakeSelections)
                {
                    updateControlGroups();

                    // If we press a mouse button, save mouse location and begin selection
                    if (selDown() || desDown() || addDown())
                    {
                        isSelecting = true;
                        //mousePosition = getMouse();
                    }


                    if (selGet())
                    {
                        RaycastHit selectionHit;
                        if (Physics.Raycast(Camera.main.ScreenPointToRay(getMouse()), out selectionHit))
                        {
                            RaycastHit hoverHitSelection = getHoveredObjects();
                            parseGameObjectForSelection(hoverHitSelection.transform.gameObject);
                        }
                    }

                    if (selUp() || desUp() || addUp() || selModeUp())
                    {
                        if (isSelecting)
                        {
                            // Wipe selection if left unclick
                            //if (selUp() || selModeUp() && selGet())
                            //{
                            //    ClearSelection();
                            //}

                            //List<GameObject> newSelectedObjects = getSelectables();
                            //// Get objects in box select
                            //foreach (GameObject selectedObject in newSelectedObjects)
                            //{
                            //    if (IsWithinSelectionBounds(selectedObject))
                            //    {
                            //        parseGameObjectForSelection(selectedObject);
                            //    }
                            //}
                        }

                        isSelecting = false;

                        // Get objects directly below mouse
                        //RaycastHit selectionHit;
                        //if (Physics.Raycast(Camera.main.ScreenPointToRay(getMouse()), out selectionHit))
                        //{
                        //    parseGameObjectForSelection(hoverHitSelection.transform.gameObject);
                        //}
                    }
                }

                #region Remove items not being hovered
                for (int x = 0; x < hoveredGameObjects.Count; x++)
                {
                    detachHover(hoveredGameObjects[x]);
                }
                hoveredGameObjects = hoveredGameObjects.Where(i => i.isActive).ToList();
                #endregion
            }
        }
    }

    #region Private Methods

    void updateControlGroups()
    {
        bool setControlGroup = false;
        bool addControlGroup = false;

        // Check for 'set' key press
        if (Input.GetKey(keySetControlGroup))
        {
            setControlGroup = true;
        }

        // Check for 'add' key press
        if (Input.GetKey(keyAddToControlGroup))
        {
            addControlGroup = true;
        }

        // Check for num key press
        int pressedNumber = -1;
        for (int i = 0; i < 10; ++i)
        {
            if (Input.GetKeyUp("" + i))
            {
                pressedNumber = i;
            }
        }

        // Set control group
        if (setControlGroup && pressedNumber >= 0)
        {
            controlGroupUnits = controlGroupUnits.Where(i => i.controlGroup != pressedNumber).ToList();
            foreach (SelectableObject selectableObject in selectedGameObjects)
            {
                controlGroupUnits.Add(new ControlGroupUnit(selectableObject, pressedNumber));
            }
        }

        // Add to control group
        else if (addControlGroup && pressedNumber >= 0)
        {
            foreach (SelectableObject selectableObject in selectedGameObjects)
            {
                controlGroupUnits.Add(new ControlGroupUnit(selectableObject, pressedNumber));
            }
        }

        // Select control group
        else if (pressedNumber >= 0)
        {
            ClearSelection();
            foreach (ControlGroupUnit controlGroupUnit in controlGroupUnits)
            {
                if (controlGroupUnit.controlGroup == pressedNumber)
                {
                    AddGameObjectToSelection(controlGroupUnit.selectableObject.gameObject);
                }
            }
        }
    }

    RaycastHit getHoveredObjects()
    {
        // Check for gameobjects directly under cursor
        RaycastHit hoverHitSelection;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(getMouse()), out hoverHitSelection))
        {
            // parseGameObjectForHover(hoverHitSelection.transform.gameObject);
        }

        if (isSelecting)
        {
            foreach (Transform transform in unitsContainer.transform)
            {
                if (IsWithinSelectionBounds(transform.gameObject))
                {
                    // parseGameObjectForHover(transform.gameObject);
                }
            }
        }
        return hoverHitSelection;
    }

    void setProjectors()
    {
        for (int x = 0; x < hoveredGameObjects.Count; x++)
        {
            SelectableObject hoveredObject = hoveredGameObjects[x];
            hoveredObject.isActive = false;
            if (hoveredObject.projector)
            {
                if (hoveredObject.selectable && hoveredObject.selectable.selectionSize > 0)
                {
                    hoveredObject.projector.orthographicSize = hoveredObject.selectable.selectionSize + hoveredObject.selectable.selectionSize / 5;
                } 
                if (disabledObjects.Contains(hoveredObject.gameObject)) detachHover(hoveredObject);
            }
            hoveredGameObjects[x] = hoveredObject;
        }
        for (int x = 0; x < selectedGameObjects.Count; x++)
        {
            SelectableObject selectedObject = selectedGameObjects[x];
            if (selectedObject.projector)
            {
                if (selectedObject.selectable && selectedObject.selectable.selectionSize > 0)
                {
                    selectedObject.projector.orthographicSize = selectedObject.selectable.selectionSize;
                }
                if (disabledObjects.Contains(selectedObject.gameObject)) detachSelection(selectedObject);
            }
            selectedGameObjects[x] = selectedObject;
        }
    }

    // Returns [filtered] game objects that are in units container
    List<GameObject> getSelectables()
    {
        List<GameObject> newSelectedObjects = new List<GameObject>();
        bool isFiltered = false;

        if (selectByName && selectedName.Length > 0)
        {
            isFiltered = true;
            newSelectedObjects.AddRange(Utils.FindGameObjectsWithName(selectedName, unitsContainer.transform));
        }

        if (!isFiltered && newSelectedObjects.Count == 0)
        {
            newSelectedObjects.AddRange(Utils.FindGameObjectsInTransform(unitsContainer.transform));
        }
        newSelectedObjects = newSelectedObjects.Where(i => !disabledObjects.Any(j => j == i)).ToList();
        return newSelectedObjects;
    }

    void detachHover(SelectableObject hoveredObject)
    {
        if (!hoveredObject.isActive)
        {
            Destroy(hoveredObject.projectorGameObject);
            if (hoveredObject.selectable)
            {
                hoveredObject.selectable.OnEndHover();
            }
        }
    }

    void detachSelection(SelectableObject selectedObject)
    {
        Destroy(selectedObject.projectorGameObject);
        if (selectedObject.selectable)
        {
            selectedObject.selectable.OnEndSelection();
        }
    }

    void parseGameObjectForHover(GameObject sender)
    {

        bool validObject = true;

        if (disabledObjects.Contains(sender)) validObject = false;

        if (selectByName && sender.name != selectedName)
        {
            validObject = false;
        }

        if (sender.transform.parent != unitsContainer.transform)
        {
            validObject = false;
        }

        if (validObject)
        {
            bool containsObject = false;
            for (int x = 0; x < hoveredGameObjects.Count; x++)
            {
                SelectableObject hoveredObject = hoveredGameObjects[x];

                if (hoveredObject.gameObject == sender)
                {
                    containsObject = true;
                    hoveredObject.isActive = true;
                }
                hoveredGameObjects[x] = hoveredObject;
            }

            if (!containsObject)
            {
                var selectable = sender.GetComponent<SelectableUnit>();
                if (selectable && selectable != selectable.playerOwned)
                {
                    return;
                }

                GameObject newHoverObject = Instantiate(hoverProjector, sender.transform.position, hoverProjector.transform.rotation) as GameObject;

                SelectableObject hoveredGameObject = new SelectableObject();
                hoveredGameObject.gameObject = sender;
                hoveredGameObject.projectorGameObject = newHoverObject;
                hoveredGameObject.isActive = true;

                var projector = newHoverObject.GetComponentInChildren<Projector>();
                if (projector)
                {
                    hoveredGameObject.projector = projector;
                }

                if (selectable)
                {
                    hoveredGameObject.selectable = selectable;
                    hoveredGameObject.selectable.OnBeginHover();
                    if (projector)
                    {
                        hoveredGameObject.projector.orthographicSize = hoveredGameObject.selectable.selectionSize + hoveredGameObject.selectable.selectionSize / 5;
                    }
                }

                hoveredGameObjects.Add(hoveredGameObject);

                newHoverObject.transform.SetParent(sender.transform);
            }
        }
    }

    void parseGameObjectForSelection(GameObject sender)
    {
        bool validObject = true;

        if (disabledObjects.Contains(sender)) validObject = false;

        if (selectByName && sender.name != selectedName)
        {
            validObject = false;
        }

        if (sender.transform.parent != unitsContainer.transform)
        {
            validObject = false;
        }

        if (validObject)
        {
            bool containsObject = false;
            List<SelectableObject> objectsToRemove = new List<SelectableObject>();
            for (int x = 0; x < selectedGameObjects.Count; x++)
            {
                SelectableObject selectedObject = selectedGameObjects[x];
                if (selectedObject.gameObject == sender)
                {
                    containsObject = true;
                    selectedObject.isActive = true;
                }

                selectedGameObjects[x] = selectedObject;

                // If object is selcted, and we are deselcting, deselect the object
                if ((addUp() || (selModeUp() && addGet())) && selectedObject.gameObject == sender)
                {

                    detachSelection(selectedObject);
                    objectsToRemove.Add(selectedObject);
                }
            }

            selectedGameObjects = selectedGameObjects.Except(objectsToRemove).ToList();

            // Object isn't selected, and we're not deselecting
            if ((!containsObject && !addUp() && !addGet() || (selModeUp() && !addGet() && !containsObject)))
            {
                var selectable = sender.GetComponent<SelectableUnit>();
                if (selectable && selectable != selectable.playerOwned)
                {
                    return;
                }

                GameObject newSelectionObject = Instantiate(selectionProjector, sender.transform.position, hoverProjector.transform.rotation) as GameObject;

                SelectableObject selectedGameObject = new SelectableObject();
                selectedGameObject.gameObject = sender;
                selectedGameObject.projectorGameObject = newSelectionObject;
                selectedGameObject.isActive = true;

                var projector = newSelectionObject.GetComponentInChildren<Projector>();
                if (projector)
                {
                    selectedGameObject.projector = projector;
                }

                if (selectable)
                {
                    selectedGameObject.selectable = selectable;
                    selectedGameObject.selectable.OnBeginSelection();
                    if (projector)
                    {
                        selectedGameObject.projector.orthographicSize = selectedGameObject.selectable.selectionSize;
                    }
                }

                selectedGameObjects.Add(selectedGameObject);

                newSelectionObject.transform.SetParent(sender.transform);
            }
            //if (!containsObject &&)
        }
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            //var rect = Utils.GetScreenRect(mousePosition, getMouse());
            //Utils.DrawScreenRect(rect, new Color(0.2f, 0.8f, 0.2f, 0.25f));
            //Utils.DrawScreenRectBorder(rect, 1, new Color(0.2f, 0.8f, 0.2f));
        }
    }

    bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition, getMouse());
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }


    #endregion

    #region Control Scheme Methods

    // Change this function if not using hardware cursor
    Vector3 getMouse()
    {
        return INP_MouseCursor.position;
    }

    
    bool selUp()     { return Input.GetMouseButtonUp((int)mouseButtons.selectButton);     }
    bool selDown()   { return Input.GetMouseButtonDown((int)mouseButtons.selectButton);   }
    bool selGet()    { return Input.GetMouseButton((int)mouseButtons.selectButton);       }
    bool desUp()     { return false; }//Input.GetMouseButtonUp((int)mouseButtons.deselectButton);   }
    bool desDown()   { return false; }//Input.GetMouseButtonDown((int)mouseButtons.deselectButton); }
    bool desGet()    { return false; }//Input.GetMouseButton((int)mouseButtons.deselectButton);     }
    bool addUp()     { return false; }//Input.GetMouseButtonUp((int)mouseButtons.addButton);        }
    bool addDown()   { return false; }//Input.GetMouseButtonDown((int)mouseButtons.addButton);      }
    bool addGet()    { return false; }//Input.GetMouseButton((int)mouseButtons.addButton);          }
    bool selModeUp() { return INP_PlayerInput.getButtonUp("SelectMode");                  }


    #endregion
    #region Public Methods

    //returns a list of currently selected objects
    public GameObject[] GetSelectedObjects()
    {
        return selectedGameObjects.Select(i => i.gameObject).ToArray();
    }
    
    public void selectAll()
    {
        List<GameObject> allObjectsList = getSelectables();
        foreach (GameObject selectedObject in allObjectsList)
        {
            parseGameObjectForSelection(selectedObject);
        }
    }

    public void toggleSelectAll()
    {
        List<GameObject> allObjectsList = getSelectables();
        bool allSelected = false;
        if (selectedGameObjects.Count >= allObjectsList.Count) allSelected = true;
        if (allSelected)
        {
            ClearSelection();
        }
        else
        {
            selectAll();
        }
    }

    //returns an ACTUAL list of currently selected objects
    public List<GameObject> GetSelectedObjectsAsList()
    {
        return selectedGameObjects.Select(i => i.gameObject).ToList();
    }

    public List<GameObject> GetHoveredObjects()
    {
        return hoveredGameObjects.Select(i => i.gameObject).ToList();
    }

    //adds a new object to selection
    public void AddGameObjectToSelection(GameObject newObject)
    {
        parseGameObjectForSelection(newObject);
    }

    //removes an object from selection
    public void RemoveGameObjectFromSelection(GameObject removedObject)
    {
        foreach (SelectableObject selectedObject in selectedGameObjects)
        {
            if (selectedObject.gameObject == removedObject)
            {
                detachSelection(selectedObject);
                selectedGameObjects.Remove(selectedObject);
            }
        }
    }

    public void disableObject(GameObject objectToDisable)
    {
        disabledObjects.Add(objectToDisable);
    }

    public void enableObject(GameObject objectToEnable)
    {
        disabledObjects.Remove(objectToEnable);
    }

    public void ClearSelection()
    {
        foreach (SelectableObject selectedObject in selectedGameObjects)
        {
            detachSelection(selectedObject);
        }
        selectedGameObjects.Clear();
    }

    #endregion
}