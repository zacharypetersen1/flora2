﻿//Noah

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTH_GraphBuilder : MonoBehaviour {



    public static PTH_GraphBuilder singleton;   // static reference to the singleton instance of this class
    float initialOffset = .001f;


    //
    // upon awaking, build the graph for the PTH_Finder module
    //
    void Awake()
    {
        singleton = this;
        buildGraphForFinder();
    }



    //
    // builds a graph into the static "PTH_Finder" module
    //
    public static void buildGraphForFinder()
    {
        PTH_Finder.setGraph(singleton.buildGraph());
    }



    //
    // builds a graph based on the current state of the game
    //
    public PTH_Graph buildGraph()
    {
        PTH_Graph ConvexPoint_Graph = new PTH_Graph(1f);                  //create new graph

        //GameObjects with PolygonCollider2D
        GameObject[] rock_polygons;
        GameObject[] dirt_polygons;
        GameObject[] structure_polygons;
        //CHANGE THIS TO ALL DECORATORS IN THE SCENE
        GameObject decorator_polygons;
        rock_polygons = GameObject.FindGameObjectsWithTag("rockWall"); 
        dirt_polygons = GameObject.FindGameObjectsWithTag("dirtWall");
        structure_polygons = GameObject.FindGameObjectsWithTag("enemyStructure");
        decorator_polygons = GameObject.Find("ProtectedGrove");

        //use "getConvexPoints" to get an array of all convex points in a polygon collider
        //Add convexPoints for all rockwalls
        foreach (GameObject wall in rock_polygons)
        {
            addWallToGraph(wall.GetComponent<PolygonCollider2D>(), wall.transform, ConvexPoint_Graph);
        }

        //Add convexPoints for all dirtwalls
        foreach (GameObject wall in dirt_polygons)
        {
            addWallToGraph(wall.GetComponent<PolygonCollider2D>(), wall.transform, ConvexPoint_Graph);
        }

        //Add convexPoints for all structures
        foreach (GameObject wall in structure_polygons)
        {
            addWallToGraph(wall.GetComponent<PolygonCollider2D>(), wall.transform, ConvexPoint_Graph);
        }

        addWallToGraph(decorator_polygons.GetComponent<PolygonCollider2D>(), decorator_polygons.transform, ConvexPoint_Graph);

        //set edges for each seen node to node 
        for (int i = 0; i < ConvexPoint_Graph.Length(); ++i)
        {
            ConvexPoint_Graph.Calculate_Add_Edges(ConvexPoint_Graph[i]);
        }

        return ConvexPoint_Graph;
    }



    //
    // returns points on this collider that are convex points
    //
    private void addWallToGraph(PolygonCollider2D poly, Transform obstacleTransform, PTH_Graph Graph)
    {
       
        List<Vector2> convexPoints = new List<Vector2>();

        //track prev, curr, and next verts in polygon
        int prev_iter = poly.points.Length - 1;
        int curr_iter = 0;
        int next_iter = curr_iter + 1;

        GameObject temp = new GameObject("TempSetupGraph");

        //checks every point in polygon for concavity
        for(int i = 0; i < poly.points.Length; ++i)
        {
            if (prev_iter > poly.points.Length - 1)
            {
                prev_iter = 0;
            }
            else if (next_iter > poly.points.Length - 1)
            {
                next_iter = 0;
            }

            temp.transform.position = obstacleTransform.position;
            temp.transform.rotation = obstacleTransform.rotation;
            temp.transform.Translate(new Vector3(poly.points[prev_iter].x, poly.points[prev_iter].y, 0));
            Vector2 prev = new Vector2(temp.transform.position.x, temp.transform.position.y);

            temp.transform.position = obstacleTransform.position;
            temp.transform.rotation = obstacleTransform.rotation;
            temp.transform.Translate(new Vector3(poly.points[curr_iter].x, poly.points[curr_iter].y, 0));
            Vector2 curr = new Vector2(temp.transform.position.x, temp.transform.position.y);

            temp.transform.position = obstacleTransform.position;
            temp.transform.rotation = obstacleTransform.rotation;
            temp.transform.Translate(new Vector3(poly.points[next_iter].x, poly.points[next_iter].y, 0));
            Vector2 next = new Vector2(temp.transform.position.x, temp.transform.position.y);

            Destroy(temp);

            //NOTE: use "isConvex" to check if a point is a convex point
            if (!isConcave(prev, curr, next))
            {
                //get 2 edges of the 3 vertices
                Vector2 left = (prev - curr).normalized;
                Vector2 right = (next - curr).normalized;

                //average normalized vectors
                Vector2 average = (left + right).normalized;

                //inverse & offset
                Vector2 inverse_average = average * -1;

                PTH_Graph.Node tempNode = new PTH_Graph.Node(curr.x, curr.y, inverse_average);
                tempNode.offsetSelf(Graph.getOffsetMagnitude());

                //checks if points doesnt overlap wall collider
                if (!poly.OverlapPoint(tempNode.getVector()))
                {
                    Graph.Add(tempNode);
                }
                
            }
            ++curr_iter;
            ++next_iter;
            ++prev_iter;
        }
        return;
    }



    //
    // checks if this point is a convex point
    //
    private bool isConcave(Vector2 prevPoint, Vector2 curPoint, Vector2 nextPoint)
    {
        Vector2 left = new Vector2(curPoint.x - prevPoint.x, curPoint.y - prevPoint.y);
        Vector2 right = new Vector2(nextPoint.x - curPoint.x, nextPoint.y - curPoint.y);

        //calculate cross product of edge vertices
        float cross = (left.x * right.y) - (left.y * right.x);
        
        //returns true if concave
        return !(cross < 0);
    }
}
