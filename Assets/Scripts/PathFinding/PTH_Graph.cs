﻿//Noah

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTH_Graph {



    List<Node> nodes;       // each node's id corresponds to its index in this list
    int node_id = 0;
    public float offsetMagnitude = 1f;
	


    //
    // represents an edge on the graph, the starting node is whatever node this edge is stored in
    //
    public struct Edge
    {
        // constructor
        public Edge(Node setOtherNode, float setDist)
        {
            otherNode = setOtherNode;
            dist = setDist;
        }

        //get distance
        public float getDist()
        {
            return this.dist;
        }

        //get node attatched to edge
        public Node getOtherNode()
        {
            return otherNode;
        }

        Node otherNode;     // the node that resides on the other end of this edge
        float dist;         // distance that must be traveled to reach the node at the other side of this edge         
    }



    //
    // represents a node on the graph, contains list of edges to all nodes that are connected to this node
    //
    public class Node
    {
        float x;            // x position of this node
        float y;            // y position of this node
        List<Edge> edgeList;    // edges to all nodes that this node is connected to
        int id;             // corresponds to this node's index in the "nodes" array
        Vector2 offsetDirection;

        // constructor
        public Node(float setX, float setY, Vector2 setOffsetDirection)
        {
            x = setX;
            y = setY;
            offsetDirection = setOffsetDirection;
            edgeList = new List<Edge>();
        }

        // adds and edge to edgeList
        public void addEdge(Edge e)
        {
            edgeList.Add(e);
        }
        
        // returns edges as an array
        public Edge[] getEdges()
        {
            return edgeList.ToArray();
        }

        //returns x,y position of node
        public Vector2 getVector()
        {
            return new Vector2(x, y);
        }

        public void setId(int newID)
        {
            id = newID;
        }

        //return id of node
        public int getID()
        {
            return id;
        }

        public void offsetSelf(float offsetMagnitude)
        {
            x += offsetDirection.x * offsetMagnitude;
            y += offsetDirection.y * offsetMagnitude;
        }

        public Vector2 getNodePosWithOffset(float offsetMagnitude)
        {
            return getVector() + offsetDirection * offsetMagnitude;
        }
    }



    //
    // constructor
    //
    public PTH_Graph(float setOffsetMagnitude)
    {
        nodes = new List<Node>();
        offsetMagnitude = setOffsetMagnitude;
    }

    public float getOffsetMagnitude()
    {
        return offsetMagnitude;
    }

    //Graph nodes are iterable with []
    public PTH_Graph.Node this[int index]
    {  
        get
        {
            return nodes[index];
        }
    }

    //calculate all nodes edges to one node
    public void Calculate_All_To_One_Edges(PTH_Graph.Node n)
    {
        for (int i = 0; i < nodes.Count; ++i)
        {
            //CHANGE FLOAT VALUE TO USE GRAPH OFFSET
            if (UTL_Dungeon.checkLOSWallsThickness(nodes[i].getVector(), n.getVector(), .8f))
            {
                nodes[i].addEdge(new PTH_Graph.Edge(n, Distance(n.getVector(), nodes[i].getVector())));
            }
        }
        return;
    }

    //calculate all nodes edges to one node
    public void Calculate_Edges_To_End(PTH_Graph.Node n)
    {
        for (int i = 0; i < nodes.Count; ++i)
        {
            //CHANGE FLOAT VALUE TO USE GRAPH OFFSET
            if (UTL_Dungeon.checkLOSWalls(nodes[i].getVector(), n.getVector()))
            {
                nodes[i].addEdge(new PTH_Graph.Edge(n, Distance(n.getVector(), nodes[i].getVector())));
            }
        }
        return;
    }

    //calculates one node edges to all nodes
    public void Calculate_Add_Edges(PTH_Graph.Node n)
    {
        
        for (int j = 0; j < nodes.Count; ++j)
        {
            
            //if same node, skip
            if (nodes[j].getID() == n.getID()) { continue; }

            //CHANGE FLOAT VALUE TO USE GRAPH OFFSET
            if (UTL_Dungeon.checkLOSWallsThickness(n.getVector(), nodes[j].getVector(), .8f))
            {
                n.addEdge(new PTH_Graph.Edge(nodes[j], Distance(n.getVector(), nodes[j].getVector())));
            }
        }
        return;
    }

    public void Calculate_Path(PTH_Graph.Node n)
    {

        for (int j = 0; j < nodes.Count; ++j)
        {

            //if same node, skip
            if (nodes[j].getID() == n.getID()) { continue; }

            //CHANGE FLOAT VALUE TO USE GRAPH OFFSET
            if (UTL_Dungeon.checkLOSWalls(n.getVector(), nodes[j].getVector()))
            {
                n.addEdge(new PTH_Graph.Edge(nodes[j], Distance(n.getVector(), nodes[j].getVector())));
            }
        }
        return;
    }

    //distance between two vectors
    public float Distance(Vector2 node, Vector2 other_node)
    {
        float dist = Mathf.Sqrt(Mathf.Pow((other_node.x - node.x), 2) + Mathf.Pow((other_node.y - node.y), 2));
        return dist;
    }

    //add nodes to graph List
    public void Add(Node node)
    {
        node.setId(node_id);
        nodes.Add(node);
        ++node_id;
    }

    //return length of nodes List
    public int Length()
    {
        return nodes.Count;
    }

    public void RemoveAtEnd()
    {
        nodes.RemoveAt(nodes.Count - 1);
    }
}
