﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTH_Path {



    protected List<Vector2> pathPoints;
    int currentIndex = 0;



    //
    // constructor
    //
    public PTH_Path(List<Vector2> setPathPoints)
    {
        pathPoints = setPathPoints;
    }



    //
    // returns the next point on the path
    //
    public Vector2 getNextPoint()
    {
        return pathPoints[currentIndex];
    }



    //
    // returns true when the current next point is the last point in the path
    //
    public bool atLastPoint()
    {
        return currentIndex == pathPoints.Count - 1;
    }



    //
    // incriments current next point to be one point later on the path
    //
    public void incrimentPoint()
    {
        currentIndex++;
    }



    //
    // Get size of the path
    //
    public int sizeOfPath()
    {
        return pathPoints.Count;
    }

    public void concatenatePath(PTH_Path otherPath)
    {
        List<Vector2> otherPoints = otherPath.pathPoints;
        pathPoints.AddRange(otherPoints);
    }
}
