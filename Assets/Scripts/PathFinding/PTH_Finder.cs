﻿//Noah

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class PTH_Finder {



    private static PTH_Graph graph;     // call PTH_GraphBuilder.buildGraphForFinder() to load this variable



    //
    // setter for graph variable
    //
    public static void setGraph(PTH_Graph setToThis)
    {
        graph = setToThis;
    }



    //
    // finds path between point a and b using graph
    //
    public static PTH_Path findPath(Vector2 a, Vector2 b)
    {
        //A* PATHFINDING

        PriorityQueue<PTH_Graph.Node> frontier = new PriorityQueue<PTH_Graph.Node>();                               //create search PriorityQueue
        Dictionary<PTH_Graph.Node, PTH_Graph.Node> came_from = new Dictionary<PTH_Graph.Node, PTH_Graph.Node>();    //create dict of linking nodes
        Dictionary<PTH_Graph.Node, float> cost_so_far = new Dictionary<PTH_Graph.Node, float>();                    //create dict of costs of nodes

        PTH_Graph.Node start = new PTH_Graph.Node(a.x, a.y, Vector2.zero); // create start node
        PTH_Graph.Node end = new PTH_Graph.Node(b.x, b.y, Vector2.zero);   // create end node
        graph.Add(end);                                          // ADD END TO GRAPH SO START CAN ADD EDGE TO START IF SEEN
        //graph.Calculate_Add_Edges(start);                        // extend edge from start
        graph.Calculate_Path(start);
        graph.Calculate_Edges_To_End(end);                   // attach all nodes to end

        frontier.insert(start, 0.0f); //insert start node
        came_from[start] = null;      //set start node
        cost_so_far[start] = 0.0f;    //cost is free to start here

        //PLACES GAME OBJECT ON NODE LOCATIONS
        for(int i = 0; i < graph.Length(); i++)
        {
            //UTL_GameObject.cloneAtLocation("Testing/node", graph[i].getVector()); 
        }
        
        //while nodes still to explore
        while (!frontier.isEmpty())
        {
            PTH_Graph.Node current = frontier.peek();
            frontier.pop();

            if (current == end) break;

            //for each node connected to current node
            foreach (PTH_Graph.Edge next in current.getEdges()) {
                float new_cost = cost_so_far[current] + graph.Distance(current.getVector(), next.getOtherNode().getVector());

                //if not seen before and cost is next best
                if (!cost_so_far.ContainsKey(next.getOtherNode()) || new_cost < cost_so_far[next.getOtherNode()])
                {
                    cost_so_far[next.getOtherNode()] = new_cost;
                    float priority = new_cost + graph.Distance(end.getVector(), next.getOtherNode().getVector());
                    frontier.insert(next.getOtherNode(), priority);
                    came_from[next.getOtherNode()] = current;
                }
            }
        }

        //remove end node from graph once found
        graph.RemoveAtEnd();

        //Get path
        List<Vector2> path = new List<Vector2>();
        PTH_Graph.Node next_path = end;
        while (next_path != start)
        {
            path.Add(next_path.getVector());
            if (came_from.ContainsKey(next_path)) next_path = came_from[next_path];
            else return null;
        }
        path.Add(start.getVector()); //add starting node to path
        path.Reverse();              //reverse path to start at the start


        foreach (var element in GameObject.FindGameObjectsWithTag("pathfindingDebug"))
        {
            MonoBehaviour.Destroy(element);
        }

        //Prints path
        //Debug.Log("Printing Path");
        foreach (Vector2 vec in path)
        {
            //UTL_GameObject.cloneAtLocation("Testing/node", vec);
            //Debug.Log(vec);
        }
        
        
        PTH_Path final_path = new PTH_Path(path);

        return final_path;
    }
}
