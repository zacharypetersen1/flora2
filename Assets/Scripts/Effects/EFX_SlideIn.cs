﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
public class EFX_SlideIn : MonoBehaviour
{
    public bool right_enter = true; // Enter screen from right, otherwise enter from left
    public float scroll_speed = 1; // Speed of scrolling, can use for parallax

    // Final position relative to camera
    public float x_offset = 0;
    public float y_offset = 0;
    public float z_offset = 0; // Use to arrange multiple elements, lower is closer

    public float max_scroll_offset = 1;
    float scroll_offset = 0;
    float counter = 0;
    float offscreen_offset = 3;
    bool active = false;
    bool slideOut = false;
    Vector3 init_p;
    public TextMeshPro text;
    int story_counter = 0;
    string timerName = "storyNoteTimer";
    bool timerSet = false;
    GameObject noteIcon;

    List<string> stories = new List<string> {
        "Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you.",
        "It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life…", 
        "He had such a knowledge of the dark side that he could even keep the ones he cared about from dying.",
        "The dark side of the Force is a pathway to many abilities some consider to be unnatural.",
        "He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. ",
        "Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself."
    };

    string cur_story = "";

    // Use this for initialization
    void Start()
    {
        if (right_enter) init_p = new Vector3(offscreen_offset - x_offset, y_offset, 1 + z_offset);
        else init_p = new Vector3(x_offset - offscreen_offset, y_offset, 1);
        new Vector3(x_offset - offscreen_offset, y_offset, 1 + z_offset);
        transform.localPosition = init_p;
        max_scroll_offset = max_scroll_offset * scroll_speed;
        text.SetText(cur_story);
        TME_Timer.addTimer(timerName, TME_Time.type.game);
        noteIcon = GameObject.Find("NewStoryIcon");
        noteIcon.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        if (INP_PlayerInput.getButtonDown("Story"))
        {
            if (active)
            {
                active = false;
                slideOut = true;
                TME_Manager.setTimeScalar(TME_Time.type.game, 1);
                CAM_Free.frozen = false;
            }
            else
            {
                active = true;
                slideOut = false;
                TME_Manager.setTimeScalar(TME_Time.type.game, 0);
                CAM_Free.frozen = true;
            }
        }
        if (active)
        {
            set_position(counter);
            if (counter < 1) counter += .02f;
        }
        if (slideOut)
        {
            set_position(counter);
            if (counter > 0) counter -= .02f;
        }

        // Notification icon
        if (timerSet)
        {
            if (TME_Timer.timeIsUp(timerName))
            {
                timerSet = false;
                noteIcon.SetActive(false);
            }
        }
    }


    public void set_position(float c)
    {
        scroll_offset -= Input.GetAxis("Mouse ScrollWheel") * scroll_speed;
        scroll_offset = Mathf.Max(0, scroll_offset);
        scroll_offset = Mathf.Min(max_scroll_offset, scroll_offset);
        var cam_p = transform.parent.transform.position;
        var x = easeOut(c);
        Vector3 tar_p;
        if (right_enter) tar_p = new Vector3(offscreen_offset * (1 - x) - x_offset, y_offset + scroll_offset, 1 + z_offset);
        else tar_p = new Vector3(x_offset - offscreen_offset * (1 - x), y_offset + scroll_offset, 1 + z_offset);
        var tar_p_lerped = Vector3.Lerp(transform.localPosition, tar_p, Time.deltaTime * 8);
        transform.localPosition = tar_p_lerped;
    }


    public static float easeOut(float x)
    {
        return -(2.5f) * Mathf.Pow(x, 3) + (3.5f) * Mathf.Pow(x, 2);
    }


    public void incrementStory()
    {
        story_counter = Mathf.Min(story_counter + 1, stories.Count);
        Debug.Log(story_counter);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < story_counter; i++)
        {
            stringBuilder.Append(stories[i]);
        }
        text.SetText(stringBuilder.ToString());
        enableNotification();
    }

    void enableNotification()
    {
        timerSet = true;
        TME_Timer.setTime(timerName, 5);
        noteIcon.SetActive(true);
    }
}