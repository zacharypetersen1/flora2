﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_PlayerPurification : MonoBehaviour
{

    ParticleSystem greenPS;
    ParticleSystem.EmissionModule greenEmitter;
    ParticleSystem.ShapeModule greenShape;

    bool purifying;
    [HideInInspector]
    public float amountPurified;

    // Use this for initialization
    void Start()
    {
        setUpParticleSystems();
        purifying = false;
    }

    void setUpParticleSystems()
    {
        greenPS = gameObject.transform.Find("GreenPurity").GetComponentInChildren<ParticleSystem>();
        greenEmitter = greenPS.emission;
        greenShape = greenPS.shape;

        greenEmitter.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void onPurifyStart()
    {
        greenEmitter.enabled = true;
        purifying = true;
    }

    public void onPurifyCancel()
    {
        //darkEmitter.enabled = false;
        greenEmitter.enabled = false;
        purifying = false;
    }

    public void onPurifyFinish()
    {
        greenEmitter.enabled = false;
        purifying = false;
    }
}
