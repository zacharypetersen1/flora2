﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_Zone {

    ArrayList myMinions;
    public AIC_Zone[] myNeighbors;
    bool _enabled = true;
    GameObject indicator;
    public int x;
    public int y;
    bool debug = false;
    public bool enabled
    {
        get { return _enabled; }
        set
        {
            setEnabled(value);
        }
    }

    public AIC_Zone(int x, int y)
    {
        myMinions = new ArrayList();
        myNeighbors = new AIC_Zone[8]; //number of possible neighbors
        this.x = x;
        this.y = y;
    }

    public void addMinionToZone(GameObject minion)
    {
        myMinions.Add(minion);
    }

    public void removeMinionFromZone(GameObject minion)
    {
        myMinions.Remove(minion);
    }

    public void setEnabled(bool enable)
    {
        if (debug)
        {
            if (enable == true)
            {
                if (indicator == null)
                {
                    indicator = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    indicator.transform.position = new Vector3(x * 128, 100, y * 128);
                    indicator.transform.localScale = new Vector3(100, 100, 100);
                    indicator.GetComponent<Collider>().enabled = false;
                }
            }
            else
            {
                if (indicator != null) GameObject.Destroy(indicator);
            }
        }
        if (_enabled == enable) return;
        _enabled = enable;
        foreach (GameObject minion in myMinions)
        {
            minion.SetActive(enable);
            minion.GetComponent<ENT_Body>().SetPSEnabled(false);
            Collider col = minion.GetComponent<Collider>();
            col.enabled = false;
            col.enabled = true;
        }
    }

    public bool isNeighbor(AIC_Zone zone)
    {
        foreach (AIC_Zone z in myNeighbors)
        {
            if (z == zone) return true;
        }
        return false;
    }
}
