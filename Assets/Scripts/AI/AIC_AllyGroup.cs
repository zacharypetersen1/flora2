﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIC_AllyGroup
{
    PLY_AllyManager.allyState state;
    public PLY_AllyManager.allyState State
    {
        get { return state; }
    }
    List<GameObject> allyList;
    public List<GameObject> AllyList
    {
        get { return new List<GameObject>(allyList); }
    }
    Vector3 position;
    PLY_AllyManager am;
    GameObject target;
    public GameObject Target
    {
        get { return target; }
    }
    float farFollowDistance;
    Vector3 midpoint;

    public AIC_AllyGroup(PLY_AllyManager.allyState newState, List<GameObject> myList, PLY_AllyManager allGroups, Vector3 pos, GameObject attackTarget = null)
    {
        state = newState;
        allyList = myList;
        am = allGroups;
        position = pos;
        target = attackTarget;

    }

    public void sortList()
    {
        if (allyList.Count <= 0) return;
        switch (state)
        {
            case PLY_AllyManager.allyState.follow:
                position = am.transform.position;
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.wait:
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.goTo:
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.activate:
                //am.compareToPos = position;
                //allyList.Sort(am.compareByDist);
                break;
            case PLY_AllyManager.allyState.attack:
                //something has to update pos here
                position = target.transform.position;
                allyList.Sort(compareByDist);
                break;

        }
    }

    public GameObject this[int key]
    {
        get
        {
            return allyList[key];
        }
    }

    public int getCount()
    {
        return allyList.Count;
    }

    public void addToList(GameObject ally)
    {
        allyList.Add(ally);
    }

    public void removeFromList(GameObject ally)
    {
        //Debug.LogError("removing " + ally.name + " from group");
        if (allyList.Contains(ally))
        {
            bool res = allyList.Remove(ally);
            if (!res) Debug.Log("Couldn't remove " + ally.name + " from list");
        }
        if (state != PLY_AllyManager.allyState.follow && allyList.Count <= 0)
        {
            bool res = am.commandGroups.Remove(this);
            if (!res) Debug.Log("Couldn't remove " + allyList + " from command Groups");
        }
    }


    public int compareByDist(GameObject a, GameObject b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a.transform.position, position);
        float distB = Vector3.Distance(b.transform.position, position);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }

    public int compareGoToPoints(Vector3 a, Vector3 b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a, midpoint);
        float distB = Vector3.Distance(b, midpoint);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }


    public float getFollowDistance(int index)
    {
        if (state == PLY_AllyManager.allyState.follow) return (am.nearFollowDistance / 2) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index);
        /*else if (state == PLY_AllyManager.allyState.goTo || state == PLY_AllyManager.allyState.wait)
        {
            float dist = (-1 * getAverageFollowDistance()) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index);
            if (Mathf.Abs(dist) < 3) dist *= 3;
            return dist;
            //return (-1 * getAverageFollowDistance()) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index) + (2 * (1 / Mathf.Log(index)));
        }*/
        else return PLY_AllyManager.FOLLOWDISTANCESCALE * index;
    }

    public float getAverageFollowDistance()
    {
        return PLY_AllyManager.FOLLOWDISTANCESCALE * (getCount() / 2);
    }

    public void setFollowDistances()
    {
        sortList();
        //create circle size
        //generate points in circle
        //assign minions based on index to indexed points in circle
        setFarFollow();
        for (int i = 0; i < getCount(); ++i)
        {
            GameObject ally = allyList[i];
            ENT_Body body = ally.GetComponent<ENT_Body>();
            body.myFollowDistance = getFollowDistance(i);
            body.farFollowRadius = farFollowDistance;
        }
        //printFollowDistances();
    }

    public void printFollowDistances()
    {
        string output = "AllyGroup contains: ";
        foreach (GameObject ally in allyList)
        {
            output += ally.name + " : " + ally.GetComponent<ENT_Body>().myFollowDistance + ", ";
        }
        Debug.Log(output);
    }

    public void assignGoToPoints(List<Vector3> goToPoints)
    {
        sortList();
        midpoint = getMidpoint();
        //sort goToPoints
        goToPoints.Sort(compareGoToPoints);
        for (int i = 0; i < goToPoints.Count; i++)
        {
            allyList[i].GetComponent<ENT_Body>().myGoToPos = goToPoints[goToPoints.Count - (i + 1)];
        }
    }

    //assumes position is set
    public Vector3 makeGoToPoint(float angle, float dist)
    {
        Vector3 goToPoint = position + UTL_Math.vectorfromAngleAndMagnitude(angle, dist);
        goToPoint = UTL_Dungeon.snapPositionToTerrain(goToPoint);
        //check if point is valid
        NavMeshHit hit;
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        if (NavMesh.SamplePosition(goToPoint, out hit, 5.0F, navmeshMask))
        {
            return hit.position;
        }
        else return new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    }

    //assumes position and allyList.Count is set
    public List<Vector3> getGoToPoints()
    {
        float startAngle = Random.Range(0F, 360F);
        List<Vector3> goToPoints = new List<Vector3>();
        Vector3 infinty = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
        if (allyList.Count == 1)
        {
            goToPoints.Add(position);
            return goToPoints;
        }

        float dist = 1.5F * 3F; //1.5 times navmesh diameter
        bool offset = false;
        for (int pointsAssigned = 0; pointsAssigned < allyList.Count; offset = !offset)
        {
            for (float angle = 0F; angle <= 360F; angle += 60F)
            {
                float tempAngle = angle + (offset ? 30 : 0) + startAngle;
                while (tempAngle >= 360) tempAngle -= 360;
                Vector3 goToPoint = makeGoToPoint(tempAngle, dist);
                if (goToPoint != infinty)
                {
                    goToPoints.Add(goToPoint);
                    pointsAssigned++;
                    if (pointsAssigned >= allyList.Count) break;
                }
            }
            dist += 1.5f;
        }
        return goToPoints;
    }

    public Vector3 getMidpoint()
    {
        Vector3 tempVec = Vector3.zero;
        int ally_count = 0;
        foreach (GameObject ally in allyList)
        {
            ++ally_count;
            tempVec += ally.transform.position;
        }
        tempVec /= ally_count;
        return tempVec;
    }


    public void setFarFollow()
    {
        //farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        if (state == PLY_AllyManager.allyState.follow) farFollowDistance = Mathf.Max(50, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        else farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
    }

    
}