﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class UTL_Dungeon {

    public static readonly int max_x = 550;
    public static readonly int max_y = 550;
    public static readonly int min_x = 0;
    public static readonly int min_y = 0;
    static Vector3 defaultTallestPoint = new Vector3(-100F, -100F, -100F);
    static Vector3 vec1 = new Vector3(0, 0, 0);
    static Vector3 vec2 = new Vector3(0, 0, 0);

    //
    // checks if objects are within line of sight
    // Harold
    //
    public static bool checkLOS(Vector3 vA, Vector3 vB, string[] obstructionTags)
    {
        float distance = Vector3.Distance(vA, vB);
        Vector3 direction = (vB - vA).normalized;
        RaycastHit[] hits = Physics.RaycastAll(vA, direction, distance);
        for (int i = 0; i < hits.Length; ++i) {
            RaycastHit hit = hits[i];
            string tag = hit.collider.gameObject.tag;
            if(obstructionTags.Contains(tag))
            {
                return false;
            }
        }
        return true;
    }



    //
    // wrapper for line of sight that checks if any walls are obstructing
    //
    public static bool checkLOSWalls(Vector3 posA, Vector3 posB)
    {
        return checkLOS(posA, posB, new string[] { "dirtWall", "rockWall" });
    }



    //
    // checks if objects are within line of sight with a ray cast thickness
    //
    public static bool checkLOSThickness(Vector3 vA, Vector3 vB, string[] obstructionTags, float radius)
    {
        float distance = Vector3.Distance(vA, vB);
        Vector3 direction = (vB - vA).normalized;
        RaycastHit[] hits = Physics.SphereCastAll(vA, radius, direction, distance);
        for (int i = 0; i < hits.Length; ++i)
        {
            RaycastHit hit = hits[i];
            string tag = hit.collider.gameObject.tag;
            if (obstructionTags.Contains(tag))
            {
                return false;
            }
        }
        return true;
    }



    //
    // wrapper for line of sight that checks if any walls are obstructing with thick ray cast
    //
    public static bool checkLOSWallsThickness(Vector3 posA, Vector3 posB, float radius)
    {
        return checkLOSThickness(posA, posB, new string[] { "dirtWall", "rockWall" }, radius);
    }



    //
    //
    //
    /*
    public statuc void checkBoundsWalls(Vector2 position)
    {
        return Collider2D
    }
    */



    //
    // Converts to correct position offset by terrain height
    //
    public static Vector3 snapPositionToTerrain(Vector3 position)
    {
        position.y = Terrain.activeTerrain.SampleHeight(position);
        return position;
    }


    //
    // Converts world position to terrain position which is in range [0,1]
    //
    public static Vector2 worldToTerrainPosition(Vector3 worldPosition)
    {
        // subtract terrain transform
        Vector2 terrainPosition = new Vector2(worldPosition.x - Terrain.activeTerrain.transform.position.x, worldPosition.z - Terrain.activeTerrain.transform.position.z);

        // divide by terrain size
        terrainPosition /= Terrain.activeTerrain.terrainData.size.x;
        return terrainPosition;
    }

    //probably doesn't work as intended
    public static bool pointWithinTerrain(Vector3 point)
    {
        RaycastHit hit;
        Ray ray = new Ray(new Vector3(point.x, point.y, -50), new Vector3(0, 0, 1));
        if (Physics.Raycast(ray, out hit, 2.0f * 50))
        {
            return true;
        }
        else return false;
    }

    public static GameObject getNearestWithTag(Vector3 position, string tag)
    {
        GameObject[] things = GameObject.FindGameObjectsWithTag(tag);
        if (things.Length == 0) return null;
        float dist = 1000000;
        GameObject nearest = null;
        foreach (GameObject thing in things)
        {
            float tempDist = Vector3.Distance(position, thing.transform.position);
            if (tempDist<= dist)
            {
                dist = tempDist;
                nearest = thing;
            }
        }
        return nearest;
    }

    public static List<GameObject> getNearestListWithTag(Vector3 position, string tag, float distance)
    {
        GameObject[] things = GameObject.FindGameObjectsWithTag(tag);
        if (things.Length == 0) return null;
        List<GameObject> nearest = new List<GameObject>();
        foreach (GameObject thing in things)
        {
            float tempDist = Vector3.Distance(position, thing.transform.position);
            if (tempDist <= distance)
            {
                nearest.Add(thing);
            }
        }
        return nearest;
    }

    private static Dictionary<ENT_DungeonEntity.monsterTypes, GameObject> minionToPath = new Dictionary<ENT_DungeonEntity.monsterTypes, GameObject>()
    {
        { ENT_DungeonEntity.monsterTypes.flowerMinion, Resources.Load("Minions/TierOneFlowerCor") as GameObject},
        { ENT_DungeonEntity.monsterTypes.treeMinion, Resources.Load("Minions/TierOneTreeCor") as GameObject },
        { ENT_DungeonEntity.monsterTypes.mushroomMinion, Resources.Load("Minions/TierOneMushroomCor") as GameObject },
        { ENT_DungeonEntity.monsterTypes.slowMinion, Resources.Load("Minions/SlowEnemy") as GameObject },
        { ENT_DungeonEntity.monsterTypes.turretMinion, Resources.Load("Minions/TurretEnemy") as GameObject},
        { ENT_DungeonEntity.monsterTypes.necroMinion, Resources.Load("Minions/NecroEnemy") as GameObject},
        { ENT_DungeonEntity.monsterTypes.confusionMinion, Resources.Load("Minions/ConfusionEnemy") as GameObject }

    };

    public static GameObject spawnMinion(ENT_DungeonEntity.monsterTypes type, Vector3 location, bool isAlly = false, List<Vector3> patrolPoints= null)
    {
        GameObject minionToSpawn = minionToPath[type];
        location = snapPositionToTerrain(location);
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        if (NavMesh.SamplePosition(location, out hit, 10, navmeshMask))
        {
            location = hit.position;
        }
        else
        {
            Debug.Log("stupid Sample Position didn't work");
        }
        GameObject minion = UTL_GameObject.cloneAtLocation(minionToSpawn, location, false);
        ENT_Body body = minion.GetComponent<ENT_Body>();
        body.startsAsAlly = isAlly;
        NavMeshAgent agent = minion.GetComponent<NavMeshAgent>();
        if (!agent.isOnNavMesh)
        {
            minion.transform.position = location;
            agent.enabled = false;
            agent.enabled = true;
        }
        body.PatrolPoints = patrolPoints;

        //create breaking ground effect where minions spawns
        EFX_Effects.createGroundCrack(location);

        return minion;
    }

    public static GameObject spawnMinion(ENT_DungeonEntity.monsterTypes type, float x_loc, float y_loc, float z_loc, bool isAlly = false, List <Vector3> patrolPoints = null)
    {
        Vector3 location = new Vector3(x_loc, y_loc, z_loc);
        return spawnMinion(type, location, isAlly, patrolPoints);
    }

    public static Vector3 getRandomPointInCircle(Vector3 center, float radius) {
        float angle = Random.Range(0F, 355F);
        Vector3 vec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
        vec = vec * (radius * Random.Range(0F, 1F));
        return center + vec;
    }


    // Spawning functions
    public static void spawnEnemyFlower(Vector3 position)
    {
        spawnMinion(ENT_DungeonEntity.monsterTypes.flowerMinion, position);
        EFX_Effects.createGroundCrack(position);
    }

    public static void spawnEnemyMushroom(Vector3 position)
    {
        spawnMinion(ENT_DungeonEntity.monsterTypes.mushroomMinion, position);
        EFX_Effects.createGroundCrack(position);
    }

    public static void spawnEnemyTree(Vector3 position)
    {
        spawnMinion(ENT_DungeonEntity.monsterTypes.treeMinion, position);
        EFX_Effects.createGroundCrack(position);
    }

    public static void makeItInteresting(Vector3 position)
    {
        // spawn some tier one minions
        for (int i = 0; i < 2; i++)
        {
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(position, 30);
            GameObject newMinion = spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, false);
            ENT_Body body = newMinion.GetComponent<ENT_Body>();
        }
        // spawn some tier two minions
        for (int i = 0; i < 1; i++)
        {
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(position, 30);
            GameObject newMinion = spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, false);
            ENT_Body body = newMinion.GetComponent<ENT_Body>();
        }
    }
}
