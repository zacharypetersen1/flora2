﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_SlowAbility : MonoBehaviour {

    ENT_Body body;
    bool purity = false;
    public float slowAbilityRadius = 20;
    public float duration = 3;
    float timer;
    float timePerGrowth;
    int slowFlowerCount;
    public ArrayList slowFlowers;
    public bool Purity
    {
        get { return purity; }
        set { purity = value; }
    }
    
    // Use this for initialization
    void Start () {
        body = GetComponentInParent<ENT_Body>();
        slowFlowerCount = (int)(slowAbilityRadius * 2);
        slowFlowers = new ArrayList(slowFlowerCount);
        //spawnSlowFlowers(slowFlowerCount);
        timePerGrowth = duration / slowFlowerCount;
        timer = duration;
	}

    // Update is called once per frame
    void Update() {

        if (timer <= 0 || body == null || body.gameObject == null)
        {    
            Destroy(gameObject);
            
        }
        timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        int flowersToSpawn = Mathf.FloorToInt((duration - timer) / timePerGrowth) - slowFlowers.Count;
        spawnSlowFlowers(flowersToSpawn);
	}
    

    void spawnSlowFlowers(int flowerCount)
    {
        for (int i = 0; i < flowerCount; ++i)
        {
            spawnSlowFlower();
        }
    }

    void spawnSlowFlower()
    {
        GameObject slowFlower = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/SlowAbility/slowObj", UTL_Dungeon.getRandomPointInCircle(transform.position, slowAbilityRadius));
        slowFlower.transform.position = UTL_Dungeon.snapPositionToTerrain(slowFlower.transform.position);
        slowFlower.GetComponent<ENV_SlowObj>().purity = purity;
        slowFlower.transform.position -= new Vector3(0, slowFlower.GetComponent<ENV_SlowObj>().distToGrow, 0);
        slowFlowers.Add(slowFlower);
    }

}
