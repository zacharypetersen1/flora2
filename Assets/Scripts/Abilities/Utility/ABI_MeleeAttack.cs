﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MeleeAttack : MonoBehaviour
{
    public TME_Time.type timeType = TME_Time.type.game;
    public string[] collidesWithTags;
    public string[] affectsTags;
    public float DC = 10;
    public float heal = 0;
    public bool purity; //should be equal to the purity of the creator of this projectile
    float lifeTime = .15f;

    //
    // Runs on collision
    //
    void OnTriggerEnter(Collider collider)
    {
        foreach (string tag in affectsTags)
        {
            if (collider.tag == tag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    Vector3 hitForward;
                    if (collider.tag == "Player")
                    {
                        hitForward = collider.transform.right;
                    }
                    else
                    {
                        hitForward = collider.transform.forward;
                    }
                    Vector3 projectileDir = collider.transform.position - transform.position;
                    float dot = Vector3.Dot(hitForward, projectileDir);
                    if (dot >= 0) //attack came from behind
                    {
                        //print(collider.gameObject.name + " got hit from behind!");
                        other.receive_combat_message(new CBI_CombatMessenger(DC * 2, heal, 0, null, transform.position));
                        if(collider.tag == "Minion")
                        {
                            other.GetComponent<Rigidbody>().AddForce(projectileDir.normalized * 15, ForceMode.Impulse);
                         
                        }
                        UTL_GameObject.cloneAtLocation("Environment Effects/Hit", transform.position + new Vector3(0, 2f, 0), false);
                    }
                    else 
                    {
                        other.receive_combat_message(new CBI_CombatMessenger(DC, heal, 0, null, transform.position));
                        other.GetComponent<Rigidbody>().AddForce(projectileDir.normalized * 10, ForceMode.Impulse);
                    }
                }
            }
        }
    }

    public void Update()
    {
        lifeTime -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if(lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }
}
