﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MushroomAttack : MonoBehaviour {

    public ENT_MushroomBody body;

    public Collider smallCollider;
    public Collider bigCollider;
    public Collider endCollider;
    public float damagePerSecond = 1.5F;
    public bool purity;
    float timer = 0;
    ArrayList hitThisFrame = new ArrayList(5);
	// Use this for initialization
	void Start () {

        body = GetComponentInParent<ENT_MushroomBody>();
        body.mushroomAttacking = true;
        bigCollider.enabled = false;
        endCollider.enabled = false;
        purity = body.isAlly;
	}
	
	// Update is called once per frame
	void Update () {
        manageColliders();
        hitThisFrame.Clear();

        if(timer >= 5.0f)
        {
            body.mushroomAttacking = false;
            Destroy(gameObject);
        }
	}

    void manageColliders()
    {
        timer += TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (timer > 1f)
        {
            if (!bigCollider.enabled) bigCollider.enabled = true;
        }
        if (timer > 2f)
        {
            if (!endCollider.enabled) endCollider.enabled = true;
        }
        if (timer > 3.5f)
        {
            if (smallCollider.enabled) smallCollider.enabled = false;
        }
        if (timer > 4.0F)
        {
            if (bigCollider.enabled) bigCollider.enabled = false;
        }
        if (timer > 4.5F)
        {
            if (endCollider.enabled) endCollider.enabled = false;
        }
    }

    void OnTriggerStay(Collider col)
    {
        ENT_DungeonEntity entity = col.GetComponent<ENT_DungeonEntity>();
        if (entity != null)
        {
            bool otherPurity = entity.isAlly;
            if (purity != otherPurity)
            {
                if (!hitThisFrame.Contains(entity))
                {
                    float damage = damagePerSecond * TME_Manager.getDeltaTime(TME_Time.type.enemies);
                    entity.receive_combat_message(new CBI_CombatMessenger(damage, 0, 0, null, transform.position));
                    hitThisFrame.Add(entity);
                }
                
            }
        }
    }
}
