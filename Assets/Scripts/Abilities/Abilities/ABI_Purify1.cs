﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ABI_Purify1 : ABI_Ability {

    //This is how much purity progress is lost each frame on enemies whose purification has been interrupted.
    private float purityDecay = 0.045f;
    //This is how fast the player makes purification progress on the target they are purifying.
    private float purityGrowth = 0.09f;
    private float Dist = 0f;
    public static bool isPurify = false;

    //These temporary variables keeps track of who is currently being purified and references its body
    public static GameObject entityToPurify;
    private ENT_DungeonEntity entity;
    private BehaviorExecutor AI;
    private NavMeshAgent agent;
    private PLY_AllyManager am;
    EFX_Purification purificationEffect;
    EFX_PlayerPurification playerEffect;

    public List<GameObject> toBeRemoved;

    private List<ENT_DungeonEntity> allMinions;

    //This dict keeps track of all entities' purity progress. This way, you can pick up
    //roughly where you left off on purifying a minion if you get interrupted.
    Dictionary<GameObject, Vector2> purityProgresssDict = new Dictionary<GameObject, Vector2>();

    //
    // constructor
    //
    public ABI_Purify1(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
        allMinions = new List<ENT_DungeonEntity>();
        am = twig.GetComponent<PLY_AllyManager>();
        toBeRemoved = new List<GameObject>();
        playerEffect = player.GetComponentInChildren<EFX_PlayerPurification>();
    }

    public override void start()
    {
        //List<GameObject> allAllies = player.GetComponent<PLY_AllyManager>().getAllAllies;
        //List<GameObject> allEnemies = player.GetComponent<PLY_AllyManager>().getAllEnemies;
        ////Fill allMinions with DungeonEntity versions of all allies and enemies
        //foreach(GameObject ally in allAllies)
        //{
        //    allMinions.Add(ally.GetComponent<ENT_DungeonEntity>());
        //}
        //foreach (GameObject enemy in allEnemies)
        //{
        //    allMinions.Add(enemy.GetComponent<ENT_DungeonEntity>());
        //}
        ////Give them all healthbars!
        //Debug.Log("Minions should eventually have the new health bars in their prefabs.");
        //foreach (ENT_DungeonEntity ent in allMinions)
        //{
        //    GameObject child = UTL_GameObject.cloneAtLocation("FloatingUI/BackgroundBar", ent.transform.position);
        //    child.transform.parent = ent.transform;
        //}
    }

    //Called every frame
    public override void update()
    {
        //Update all health bars
        updateAllHealthBars();
        //If target changes, cancel purification
        /*if ((entityToPurify != null && UTL_Targeting.target != entityToPurify && UTL_Targeting.target != null) || CAM_Dolly.locked)
        {
            CancelPurification();
        }*/
        List<GameObject> keys = new List<GameObject>(purityProgresssDict.Keys);
        foreach (var key in keys)
        {
            if (key == null)
            {
                purityProgresssDict.Remove(key);
                continue;
            }
            if (purityProgresssDict[key][0] >= 0)
            { //Decay all values except the ones already at 0, even the target being purified.
              //The target being purified has extra growth to counteract this decay.
                purityProgresssDict[key] -= new Vector2(purityDecay,0);
                EFX_Purification effect = key.GetComponentInChildren<EFX_Purification>();
                if (effect != null)
                {
                    effect.amountPurified = (purityProgresssDict[key][1] - purityProgresssDict[key][0]) / purityProgresssDict[key][1];
                    //effect.purificationProgress = purityProgresssDict[key];
                }
            }

            if (purityProgresssDict[key][0] < 0)
            { //If purity progress has completely decayed from a minion, remove it from the dictionary
                key.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().hidePurity();
                purityProgresssDict.Remove(key);
                EFX_Purification effect = key.GetComponentInChildren<EFX_Purification>();
                if (effect != null)
                {
                    effect.onPurifyCancelFinish();
                }
            }
            else if(key)
            {
                float missing_purity = purityProgresssDict[key][1] - purityProgresssDict[key][0];
                key.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().scalePurityToValue(missing_purity, purityProgresssDict[key][1]);
            }
        }
    }


    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        //Don't purify if left clicking in select mode
        if (!SelectionManager.selectMode)
        {
            //Get current target
            GameObject check_target = UTL_Targeting.target;
            //If no current target update targeting
            if (!check_target)
            {
                check_target = UTL_Targeting.getTarget();
            }
            //If target found change camera state
            if (check_target)
            {
                //CAM_Camera.setState(CAM_State.type.targeting);
            }
            //Attempt to purify target
            if (check_target != null)
            {
                try
                {
                    BeginPurify(check_target);
                }
                catch (Exception e)
                {
                    Debug.Log("Tried to purify something that wasn't a DungeonEntity");
                    throw e;
                }
            }
        }
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        //If button is let go, cancel purification
        if (entityToPurify)
        {
             CancelPurification();
        }  
    }


    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown(float holdTime)
    {
        if (entityToPurify && !purityProgresssDict.ContainsKey(entityToPurify))
        {
            BeginPurify(entityToPurify);
        }
        //If button is being held, do Purifying
        if (entityToPurify != null/* && UTL_Targeting.target != null*/)
        {
            Dist = Vector3.Distance(player.transform.position, entityToPurify.transform.position);
            purityProgresssDict[entityToPurify] += new Vector2(purityGrowth, 0); /*FOR ALPHA, DISTANCE D/N EFFECT PURIFY*///new Vector2((purityGrowth / Dist), 0);
            purityProgresssDict[entityToPurify] += new Vector2((purityDecay), 0); //To counteract decay.
            //If successfully finished purifying, call CompletePurification.
            if (purityProgresssDict[entityToPurify][0] >= purityProgresssDict[entityToPurify][1])
            {
                CompletePurification();
            }
        }
    }

    void BeginPurify(GameObject objectToPurify)
    {
        // Set player animation
        PLY_Anim.animator.SetBool("isPurify", true);
        isPurify = true;

        // Set enemy minion animation
        objectToPurify.GetComponentInChildren<Animator>().SetBool("isPurify", true);

        //Debug.Log("Begin purify");
        entity = objectToPurify.GetComponent<ENT_DungeonEntity>();
        objectToPurify.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().showPurity();
        //enable purificationEffect
        purificationEffect = objectToPurify.GetComponentInChildren<EFX_Purification>();
        if (purificationEffect != null)
        {
            purificationEffect.onPurifyStart();
        }
        if (playerEffect != null)
        {
            playerEffect.onPurifyStart();
        }
        if (!purityProgresssDict.ContainsKey(objectToPurify))
        { //If we have never made any purification progress on this entity, add it to the dict
            purityProgresssDict[objectToPurify] = new Vector2(purityGrowth, entity.CL);
        }
        else
        { //Updates stored CL value if entity already in dict
            purityProgresssDict[objectToPurify] = new Vector2(purityProgresssDict[objectToPurify][0], entity.CL);
        } //Note Current Purity = purityProgresssDict[objectToPurify][0]
        entityToPurify = objectToPurify;
        entity.vulnerable = false;
        AI = entityToPurify.GetComponent<BehaviorExecutor>();
        agent = entityToPurify.GetComponent<NavMeshAgent>();
        if (AI != null)
        {
            AI.enabled = false;
        }
        if (agent != null)
        {
            agent.Stop();
        }
        am.removeEnemy(entityToPurify);
    }

    void CancelPurification()
    {
        // Cancel player's purify animation
        PLY_Anim.animator.SetBool("isPurify", false);
        isPurify = false;

        // Cancel enemy minion's purify animation
        entity.GetComponentInChildren<Animator>().SetBool("isPurify", false);

        if (purificationEffect != null)
        {
            purificationEffect.onPurifyCancel();
        }
        if (playerEffect != null)
        {
            playerEffect.onPurifyCancel();
        }
        //Debug.Log("Cancel puify");
        if (CAM_Camera.singleton.currentState == CAM_State.type.targeting)
        {
            //CAM_Camera.setState(CAM_State.type.returning);
        }
        if (entity && entityToPurify)
        {
            am.addEnemy(entityToPurify);
            if (AI != null)
            {
                AI.enabled = true;
                agent.Resume();
            }
            entity.vulnerable = true;
            entityToPurify = null;
            entity = null;
        }
        
    }

    void CompletePurification()
    {
        // Cancel player's purify animation
        PLY_Anim.animator.SetBool("isPurify", false);
        isPurify = false;

        // Cancel enemy minion's purify animation
        entity.GetComponentInChildren<Animator>().SetBool("isPurify", false);

        //Debug.Log("Complete purify");
        //Trigger the target's purify_self event.
        if (purificationEffect != null)
        {
            purificationEffect.onPurifyFinish();
        }
        if (playerEffect != null)
        {
            playerEffect.onPurifyFinish();
        }
        entity.purify_self();
        entity.CL = entity.CL_MAX;
        entity.vulnerable = true;
        //CAM_Camera.setState(CAM_State.type.returning);
        //Remove purity progress bar
        entityToPurify.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().scalePurityToValue(0.0f, 1.0f);
        //Remove from dictionary and reset temporary variables
        purityProgresssDict.Remove(entityToPurify);
        entityToPurify = null;
        entity = null;
        UTL_Targeting.target = null;
        if (AI != null)
        {
            AI.enabled = true;
            agent.Resume();
        }
        
    }

    
    //May be moved in the future...
    //Updates the values of all health bars
    public void updateAllHealthBars()
    {
        /*
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllAllies)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllEnemies)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllStructures)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
        */
        //Remove cleansed enemies and allies from the purity dict
        
        /*foreach (GameObject ent in purityProgresssDict.Keys)
        {
            if(!player.GetComponent<PLY_AllyManager>().getAllAllies.Contains(ent) && !player.GetComponent<PLY_AllyManager>().getAllEnemies.Contains(ent) && !player.GetComponent<PLY_AllyManager>().getAllStructures.Contains(ent))
            {
                toBeRemoved.Add(ent);
            }
        }*/
        foreach (GameObject ent in toBeRemoved)
        {
            purityProgresssDict.Remove(ent);
        }
        toBeRemoved.Clear();
    }   
}
