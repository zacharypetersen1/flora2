﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Cleansing_backup {//: ABI_Ability {
    /*
    CAM_Dolly dolly;
    Animator animator;

    float chargeTime = 3f;
    bool charging = false;

    //Vector3 direction;
    public float directionOffset = 1.2f;
    GameObject holdAttack;
    GameObject attack;
    GameObject aoe;

    public bool holdAttackActivated;
    public float holdAttackRadius = 5;
    int queBasicAttack = 0;
    int queHoldAttack = 0;
    float basicQueTimer = .3f;
    float holdQueTimer = .5f;
    
    float holdtime;
    float particlesize;
    float largerSize = 5;

    //
    // constructor
    //
    /*public ABI_Cleansing(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // runs once on 
    //
    public override void start()
    {
        dolly = GameObject.Find("CameraDolly").GetComponent<CAM_Dolly>();
        animator = GameObject.Find("TwigMesh").GetComponent<Animator>();
    }



    //
    // Get amount of recharge time for this ability
    //
    public override float getRechargeTime()
    {
        return 0.4f;
    }

    public override void update()
    {
        //Debug.Log(manager.getCurCharge(ABI_Manager.abilityType.Cleansing));
        //direction = getDirection();
        base.update();
        if (queBasicAttack > 0 && manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
        {
            basicAttack();
            queBasicAttack-=1;
        }else if (queHoldAttack > 0 && manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
        {
            basicHoldAttack();
            queHoldAttack-=1;
        }

        //holdtime = GameObject.Find("Player").GetComponent<ABI_Listener>().getHeldTimer(ABI_Manager.abilityType.Cleansing);
        //holdAttackRadius = holdtime <= 5 ? 5 : holdtime;
    } 

    /*
    //
    //Get direction of Caster (direction comes from camera dolly)
    //
    public Vector3 getDirection()
    {
        Vector3 dir = UTL_Math.angleToUnitVec(CAM_Camera.getDollyPhysicalRotZ() * Mathf.Deg2Rad);
        return dir;
    }
    


    //
    // activate basic attack hold ability
    //
    public override void buttonDown()
    {
        if (manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
        {
            basicAttack();
        }

        //check if hold attack activated
        /*if (holdAttackActivated)
        {
            //check if hold attack object is used
            if (holdAttack)
            {
                basicHoldAttackAOE();
            }
            else if (!holdAttack)
            {
                holdAttackActivated = false;
            }
        }
    }



    //
    //
    //
    public override void buttonUp()
    {
        /*if (!wasHeld)
        {
            if (manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
            {
//                RBL_RumbleManager.rumbleOverTime("basicCleanse", 0.25f, .35f);
                basicAttack();
            }
            //checks if less than checkQue seconds left and then queues another basic attack
            else if(!manager.abilityIsReady(ABI_Manager.abilityType.Cleansing) && manager.getCurCharge(ABI_Manager.abilityType.Cleansing) <= basicQueTimer)
            {
                if (queBasicAttack < 1)
                {
                    queBasicAttack += 1;
                }
            }    
        }else if(wasHeld)
        {
//            RBL_RumbleManager.turnRumbleOff("basicCleanseCharge");
            if (manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
            {
                basicHoldAttack();
            }
            else if(!manager.abilityIsReady(ABI_Manager.abilityType.Cleansing) && manager.getCurCharge(ABI_Manager.abilityType.Cleansing) <= holdQueTimer)
            {
                if (queHoldAttack < 1)
                {
                    queHoldAttack += 1;
                }
            }           
            
        }
        wasHeld = false;
    }



    public override void buttonHeld()
    {
        
        /*if (manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
        {
            wasHeld = true;
            charging = true;
        } 
    }

    public override void buttonHeldDown(float holdTime)
    {
        if (manager.abilityIsReady(ABI_Manager.abilityType.Cleansing))
        {
            basicAttack();
        }
        /*if (charging)
        {
            RBL_RumbleManager.turnRumbleOn("basicCleanseCharge", holdTime >= chargeTime ? 1 : (holdTime / chargeTime) * 0.8f + 0.2f);
        }
    }

    //
    //basic attack
    //
    public void basicAttack()
    {
        RBL_RumbleManager.rumbleOverTime("basicCleanse", 0.25f, 0.25f);
        animator.SetInteger("basicAttackType", UnityEngine.Random.Range(0, 3));
        animator.SetTrigger("basicAttack");
        attack = shootParticle("Abilities/BasicAttack/BasicAttack", driver.transform.position + direction * directionOffset);
        //attack.GetComponent<CBI_CombatMessenger>().init(4, 0, "", null, driver.gameObject);
        holdAttackActivated = false;
        manager.reduceAbilityCharge(ABI_Manager.abilityType.Cleansing);
    }



    //
    //basic hold attack
    //
    public void basicHoldAttack()
    {
        // make controller rumble

        holdAttack = shootParticle("Abilities/BasicAttack/BasicHoldAttack", driver.transform.position + direction * directionOffset);
        holdAttackActivated = true;
        manager.reduceAbilityCharge(ABI_Manager.abilityType.Cleansing);
    }



    //
    //basic hold attack AOE
    //
    public void basicHoldAttackAOE()
    {
        Vector3 loc = holdAttack.transform.position;
        MonoBehaviour.Destroy(holdAttack);
        holdAttackActivated = false;
        aoe = shootParticle("Abilities/BasicAttack/BasicHoldAttackAOE",loc);
        aoe.GetComponent<CBI_CombatMessenger>().init(4, 0, "", null, driver.gameObject);
        aoe.GetComponent<CircleCollider2D>().radius = holdAttackRadius;
        aoe.GetComponent<ABI_ParticleSystem>().radius = holdAttackRadius;
    }



    //
    //instanitate partcle and set its direction
    //
    public GameObject shootParticle(String particle, Vector3 startLoc)
    {
        GameObject obj = UTL_GameObject.cloneAtLocation(particle, startLoc);
        try
        {
            obj.GetComponent<ABI_Projectile>().setDirection(direction);
            obj.GetComponent<CBI_CombatMessenger>().sender = GameObject.Find("Player");
        }
        catch
        {

        }
        
        return obj;
    }
    */
}
