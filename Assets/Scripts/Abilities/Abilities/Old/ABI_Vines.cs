﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Vines : ABI_Ability
{
    bool activated = false;
    GameObject vineTarget;

    //
    // constructor
    //
    public ABI_Vines(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // activate TreeRoots
    //
    public override void buttonDown()
    {
        Debug.Log("Ability Button Pressed!");
        vineTarget = getTarget();
        if (vineTarget != null)
        {
            // Setup
            activated = true;
            motor.enabled = false;

            CAM_Camera.setDollyInfluence(2);
        }
    }

    public override void buttonUp()
    {
        Debug.Log("Ability Button Released!");
        if (activated)
        {

            // Cleanup
            motor.enabled = true;
            CAM_Camera.setDollyInfluence(0);
            activated = false;
        }
    }

    public override void buttonHeld()
    {
       // Debug.Log("Ability Button has now registered that it is being held!!!");
    }

    public override void buttonHeldDown(float holdTime)
    {
        //Debug.Log("Button has been held for: " + holdTime + " seconds");
        if (activated)
        {
            Debug.Log("vines still active");
        }
    }

    public GameObject getTarget()
    {
        float minD = Mathf.Infinity;
        GameObject obj = null;
        foreach (var enemyObj in GameObject.FindGameObjectsWithTag("enemy"))
        {
            float thisD = Vector2.Distance(UTL_Math.vec3ToVec2(driver.transform.position), UTL_Math.vec3ToVec2(enemyObj.transform.position));
            if (thisD < minD)
            {
                obj = enemyObj;
                minD = thisD;
            }
        }

        return obj;
    }
}