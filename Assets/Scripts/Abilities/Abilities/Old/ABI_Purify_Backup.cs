﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public class ABI_Purify : ABI_Ability {



    TME_Time.type timeType = TME_Time.type.game;
    bool attemptingPur = false;
    float curPurTime = 0;
    float maxPurTime = 2f;
    float slowedTimeScale = 0.2f;

    float minPureDist = 5f;
    GameObject pureTarget;
    GameObject particle;
    GameObject purityParticleEffect;



    //
    // constructor
    //
    public ABI_Purify(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // activate TreeRoots
    //
    public override void buttonDown()
    {
       pureTarget = getTarget();
        if(pureTarget != null)
        {
            acknowledgePure();
        }
    }

    public override void buttonUp()
    {
        resetPure();
    }

    public override void buttonHeld()
    {
        //Debug.Log("Ability Button has now registered that it is being held!!!");
    }

    public override void buttonHeldDown(float holdTime)
    {
        //Debug.Log("Button has been held for: " + holdTime + " seconds");
    }

    public override void update()
    {
        if (attemptingPur)
        {
            curPurTime += TME_Manager.getDeltaTime(TME_Time.type.master);
            if(curPurTime >= maxPurTime)
            {
                if (attemptPurify())
                {
                    runPure();
                }
                return;
            }

            TME_Manager.setTimeScalar(timeType, 1 - (1-slowedTimeScale) * (curPurTime / maxPurTime));
        }
    }



    //
    // TODO make a better way of doing this
    //
    public GameObject getTarget()
    {
        float minD = 35f;
        GameObject obj = null;
        foreach(var enemyObj in GameObject.FindGameObjectsWithTag("enemy"))
        {
            float thisD = Vector2.Distance(UTL_Math.vec3ToVec2(driver.transform.position), UTL_Math.vec3ToVec2(enemyObj.transform.position));
            if(thisD < minD)
            {
                obj = enemyObj;
                minD = thisD;
            }
        }

        return obj;
    }



    //
    // acknowledges that a valid target has been found and starts purification attempt
    //
    void acknowledgePure()
    {
        particle = UTL_GameObject.cloneAtLocation("Particles/PurifyEffect", pureTarget.transform.position);
        particle.transform.parent = pureTarget.transform;
        attemptingPur = true;
        PLY_Anim.animator.SetBool("isPurify", true);
    }



    //
    // Resets var's related to purification
    //
    void resetPure()
    {
        if(particle != null){
            ParticleSystem ps = particle.GetComponent<ParticleSystem>();
            ParticleSystem.EmissionModule em = ps.emission;
            em.rateOverTime = 0;
        }
        attemptingPur = false;
        curPurTime = 0;
        TME_Manager.setTimeScalar(timeType, 1);
        pureTarget = null;
        PLY_Anim.animator.SetBool("isPurify", false);
    }

    /// <summary>
    /// Evaluates the probability that purify succeeds
    /// </summary>
    bool attemptPurify()
    {
        float target_CL = pureTarget.GetComponent<ENT_Body>().CL;
        float target_max_CL = pureTarget.GetComponent<ENT_Body>().CL_MAX;
        if (target_CL == target_max_CL)
        {
            Debug.Log("Purify failed; Target's CL is maxed out");
            attemptingPur = false;
            TME_Manager.setTimeScalar(timeType, 1);
            return false;
        }
        else if (target_CL <= 1)
        {
            return true;
        }
        else
        {
            //Formula here
            float chance = UnityEngine.Random.Range(1.0f, 2.0f);
            if (((chance+target_max_CL-target_CL)/(target_max_CL)) > 1)
            {
                Debug.Log("Purify chance succeeded. Rolled " + (chance + target_max_CL - target_CL) + " / " + target_max_CL);
                return true;
            }
            else
            {
                Debug.Log("Purify chance failed. Rolled " + (chance + target_max_CL - target_CL) + " / " + target_max_CL);
                attemptingPur = false;
                TME_Manager.setTimeScalar(timeType, 1);
                return false;
            }
        }
    }

    //
    // Runs actual purification once it occurs
    //
    void runPure()
    {
        pureTarget.GetComponent<ENT_Body>().purify_self();
        pureTarget.GetComponent<ENT_Body>().CL = pureTarget.GetComponent<ENT_Body>().CL_MAX; //Full heal the minion upon purification
        purityParticleEffect = UTL_GameObject.cloneAtLocation("Environment Effects/purify", pureTarget.transform.position);
        purityParticleEffect.transform.parent = pureTarget.transform;
        attemptingPur = false;
        TME_Manager.setTimeScalar(timeType, 1);
        Debug.Log("Ran");
    }
}
*/