﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


//
// Handles purification "weapon"
//
public class ABI_Purify : ABI_Ability {



    // The different types of purification "attacks"
    public enum purifyTypes {
        basic, quickShot, multiShot
    }

    public purifyTypes currentType = purifyTypes.quickShot;

    // Stores instances of all purify types
    Dictionary<purifyTypes, ABI_PurifyType> typeList = new Dictionary<purifyTypes, ABI_PurifyType>{
        {purifyTypes.basic, new ABI_Basic() },
        {purifyTypes.quickShot, new ABI_QuickShot() },
        {purifyTypes.multiShot, new ABI_MultiShot() }
    };


    //
    // constructor
    //
    public ABI_Purify(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    public override void start()
    {
        
    }



    // DEBUG Listen for player to change selected purify type
    public override void update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            currentType = purifyTypes.basic;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            currentType = purifyTypes.quickShot;
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            currentType = purifyTypes.multiShot;
        }
    }



    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        if(!SelectionManager.selectMode)
        typeList[currentType].groundShotDown();
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        typeList[currentType].shotUp();
    }



    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown(float holdTime)
    {

    }
}
