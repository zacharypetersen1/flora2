﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MultiShot : ABI_PurifyType {

    public override void groundShotDown()
    {
        ABI_Nozzle.setParticleStateChildren(ABI_Nozzle.nozzle.multiShot, true);
    }

    public override void shotUp()
    {
        ABI_Nozzle.setParticleStateChildren(ABI_Nozzle.nozzle.multiShot, false);
    }

    public override void surfShotDown()
    {
        
    }
}
