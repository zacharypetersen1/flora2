﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_QuickShot : ABI_PurifyType {

    public override void groundShotDown()
    {
        ABI_Nozzle.setParticleState(ABI_Nozzle.nozzle.quickShot, true);
    }

    public override void shotUp()
    {
        ABI_Nozzle.setParticleState(ABI_Nozzle.nozzle.quickShot, false);
    }

    public override void surfShotDown()
    {
        
    }
}
