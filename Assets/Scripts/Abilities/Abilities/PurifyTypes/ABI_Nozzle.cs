﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Nozzle : MonoBehaviour {

    public GameObject basicProjectile;
    public ParticleSystem hose;
    public ParticleSystem quickShot;
    public GameObject multiShot;

    public static ABI_Nozzle nozzle;



    //
    // Set up singleton
    //
    void Start()
    {
        nozzle = this;
    }



    //
    // Emits specified projectile in forward direction from twig at specified force
    //
    public static void emitProjectile(GameObject projectile, float force)
    {
        GameObject proj = (GameObject)Instantiate(projectile);
        proj.transform.position = nozzle.transform.position;
        proj.transform.rotation = nozzle.transform.rotation;
        proj.transform.Translate(proj.transform.right * 2 + Vector3.up*2);
        proj.GetComponent<Rigidbody>().AddForce(ABI_Nozzle.nozzle.transform.right * force);
    }

    public static void setParticleState(ParticleSystem system, bool enabled)
    {
        ParticleSystem.EmissionModule em = system.emission;
        em.enabled = enabled;
    }

    public static void setParticleStateChildren(GameObject parent, bool enabled)
    {
        foreach(var system in parent.GetComponentsInChildren<ParticleSystem>())
        {
            setParticleState(system, enabled);
        }
    }
}
