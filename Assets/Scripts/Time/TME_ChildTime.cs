﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TME_ChildTime : TME_Time {



    TME_Time parent;        // child time inherits from this parent time



    //
    // Constructor
    //
    public TME_ChildTime(TME_Time setParent)
    {
        parent = setParent;
    }



    //
    // Returns the delta time based on the time scalar for this time
    //
    public override float getDeltaTime()
    {
        return parent.getDeltaTime() * scalar;
    }



    //
    // Returns the time scalar that is currently being applied to the delta time
    //
    public override float getTimeScalar()
    {
        return parent.getTimeScalar() * scalar;
    }
}
