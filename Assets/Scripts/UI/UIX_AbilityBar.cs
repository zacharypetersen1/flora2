﻿// Zach

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIX_AbilityBar : MonoBehaviour {

    ABI_Manager abilities;
    Material mat;
    public abilityColor color;          // color this button will be
    [Range(0, 7)]                       
    public int abilityNum;              // which ability number this button will represent
    private Vector3 baseScale;          // for setting scale of ui element



    //
    // for the A B X Y buttons on Xbox remote
    //
    public enum abilityColor
    {
        green, red, blue, yellow
    }



    //
    // maps abilityColor struct to corresponding colors
    //
    private Dictionary<abilityColor, Color[]> colorMap = new Dictionary<abilityColor, Color[]>
    {
        { abilityColor.green,  new Color[2] { new Color(0, 1, 0), new Color(.4f, .8f, .4f ) } },
        { abilityColor.red,    new Color[2] { new Color(1, .4f, .4f), new Color(.8f, .5f, .5f ) } },
        { abilityColor.blue,   new Color[2] { new Color(.4f, .4f, 1), new Color(.5f, .5f, .8f ) } },
        { abilityColor.yellow, new Color[2] { new Color(1, 1, 0), new Color(.7f, .7f, .5f ) } }
    };



    //
    // returns Vector3 that sizes this object to correct proportions to represent how much ability is recharged
    //
    private Vector3 scalar(float s)
    {
        return new Vector3(baseScale.x * (abilities.abilityIsReady(abilityNum) ? 1 : 0.3f), baseScale.y, baseScale.z * s);
    }



    //
    // Use this for initialization
    //
    void Start () {
        baseScale = transform.localScale;
        mat = gameObject.GetComponent<MeshRenderer>().material;
        abilities = GameObject.Find("Player").GetComponent<ABI_Manager>();
	}



    //
    // Update is called once per frame
    //
    void Update () {
        // set size of recharge meter using scalar function
        transform.localScale = scalar(abilities.getAbilityChargeScalar(abilityNum));
        mat.color = colorMap[color][ abilities.abilityIsReady(abilityNum) ? 0 : 1 ];
	}
}
