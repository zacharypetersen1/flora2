﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_ParentBar : MonoBehaviour {
    public float offset;
	// Use this for initialization
	void Start () {
        Vector3 Pos = transform.position;
        Pos.y += offset;
        transform.position = Pos;


	}
	
	// Update is called once per frame
	void Update () {
        //transform.rotation = Camera.main.transform.rotation;
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.down);
        //transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);

    }
}
