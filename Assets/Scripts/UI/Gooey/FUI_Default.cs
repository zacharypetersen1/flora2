﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_Default : MonoBehaviour {

    protected float _offset;
    public float offset
    {
        get { return _offset; }
        set { _offset = value;
            setSpritePos();
        }
    }
    public float Offset = 3.5F;
    protected SpriteRenderer rend;
    protected Camera cam;

    // Use this for initialization
    protected virtual void Awake () {
        rend = GetComponent<SpriteRenderer>();
        cam = Camera.main;
        _offset = Offset;
	}

    protected virtual void Start()
    {
        setSpritePos();
    }

    // Update is called once per frame
    protected virtual void Update () {
        LookAtCamera();
	}

    public void show()
    {
        rend.enabled = true;
    }

    public void hide()
    {
        rend.enabled = false;
    }

    void setSpritePos()
    {
        if (transform.parent != null) transform.position = transform.parent.transform.position + new Vector3(0, offset, 0);
    }

    void LookAtCamera()
    {
        transform.LookAt(cam.transform.position);
    }
}
