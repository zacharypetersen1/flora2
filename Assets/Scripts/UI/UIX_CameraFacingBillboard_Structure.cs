﻿// Ben, Matt, Zach

using UnityEngine;
using System.Collections;

/*
 * DEPRECATED
 */
public class UIX_CameraFacingBillboard_Structure : MonoBehaviour
{
    public Camera m_Camera;
    public RectTransform CL;

    public GameObject structObject;
    private ENT_Structure structure;

    void Start()
    {
        structure = structObject.GetComponent<ENT_Structure>();
        if(m_Camera == null)
        {
            m_Camera = GameObject.Find("Camera").GetComponent<Camera>();
        }
        //CL = GameObject.Find("CL").GetComponent<RectTransform>();
        // brain.MaxHp = 100;
        // brain.Health = 50;
    }

    void Update()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);

        updateHealthBar(structure.CL_MAX, structure.CL);
    }

    void updateHealthBar(float maxHealth, float currentHealth)
    {
        // normalize
        CL.sizeDelta = new Vector2((currentHealth / maxHealth) * 100, 100);

    }

}