﻿// Ben
// https://www.sitepoint.com/adding-pause-main-menu-and-game-over-screens-in-unity/
// http://answers.unity3d.com/questions/1173303/how-to-check-which-scene-is-loaded-and-write-if-co.html
// http://answers.unity3d.com/questions/1011523/first-selected-gameobject-not-highlighted.html

// Had to manually include a few so editor wouldn't complain
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIX_Pause : MonoBehaviour
{

    EventSystem es;
    GameObject[] pauseObjects;
    GameObject[] controls;

    GameObject controlButton;

    //bool controlsClicked = false;
    bool paused = false;
    string sceneName;

    // Use this for initialization
    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        pauseObjects = GameObject.FindGameObjectsWithTag("showOnPause");
        controls = GameObject.FindGameObjectsWithTag("showOnControls");

        controlButton = GameObject.Find("Back_Button");

        es = GameObject.Find("EventSystem").GetComponent<EventSystem>();

        hidePaused();
        initialHideControls();
        showPaused(); // hiding the UI initially doesn't work so I had to do this. Sorry
        initialShowControls();
        hidePaused();
        initialHideControls();
    }

    // Update is called once per frame
    void Update()
    {
        if (INP_PlayerInput.getButtonDown("Pause"))
        {
            if (!paused) // STOP TIME
            {
                TME_Manager.setTimeScalar(TME_Time.type.game, 0);
                showPaused();
            }
            else if (paused) // RESUME TIME
            {
                TME_Manager.setTimeScalar(TME_Time.type.game, 1);
                hidePaused();
                initialHideControls();
            }

            paused = !paused;
        }
    }

    public void Resume()
    {
        TME_Manager.setTimeScalar(TME_Time.type.game, 1);
        hidePaused();
    }

    public void Reload()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        SceneManager.LoadScene(0);
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
        // shows initial menu
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(es.firstSelectedGameObject);
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        // hides initial menu
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    public void showControls()
    {
        hidePaused();
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(true);
        }
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(controlButton);
    }

    public void hideControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(false);
        }
        showPaused();
    }

    public void initialShowControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(true);
        }
    }

    public void initialHideControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(false);
        }
    }

}

