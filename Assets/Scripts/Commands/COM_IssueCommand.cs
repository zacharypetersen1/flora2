﻿// Matt & Ben
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COM_IssueCommand : MonoBehaviour
{
    GameObject commandObject;
    Vector3 commandObjectHome;

    Camera worldCamera = new Camera();

    COM_AllyCommands playerCommands;
    UIX_Crosshair crosshair;

    ENT_SplicerController splicerController;

    SEL_Manager selectMan;
    List<GameObject> selected;
    PLY_AllyManager allyMan;
    List<List<GameObject>> allAllies;
    List<GameObject> slowAllies;
    List<GameObject> turretAllies;
    List<GameObject> necroAllies;
    List<GameObject> confusionAllies;
    int groupCount = 0;

    SFX_Minions minionsounds;

    GameObject player;
    public float groupUpRadius = 5;

    private bool singleClick = false;
    private float doubleClickTimeLimit = 0;
    public float delay = 0.2f;

    //Dictionary<int, int> dict;
    

    // Use this for initialization
    void Start()
    {
        commandObject = GameObject.Find("CommandPointObject");
        commandObjectHome = commandObject.transform.position;

        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();

        selectMan = GameObject.Find("Scripts").GetComponent<SEL_Manager>();
        allyMan = GameObject.Find("Player").GetComponent<PLY_AllyManager>();
        minionsounds = GameObject.Find("Music").GetComponent<SFX_Minions>();
        playerCommands = GameObject.Find("Player").GetComponent<COM_AllyCommands>();

        crosshair = GameObject.Find("Crosshair").GetComponent<UIX_Crosshair>();
        splicerController = GameObject.Find("SplicerController").GetComponent<ENT_SplicerController>();

        allAllies = allyMan.getAllies();
        slowAllies = new List<GameObject>(allAllies[3]);
        turretAllies = new List<GameObject>(allAllies[4]);
        necroAllies = new List<GameObject>(allAllies[5]);
        confusionAllies = new List<GameObject>(allAllies[6]);

        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // right click
        if (INP_PlayerInput.getButtonDown("Command"))
        {
            determineClick();
        }

        if (INP_PlayerInput.getButtonDown("FollowCommand") || INP_PlayerInput.getButtonDown("FollowCommand2"))
        {
            alternativeFollowCommand();
        }

        if ((Time.time - doubleClickTimeLimit) > delay)
        {
            if (singleClick)
            {
                switch (crosshair.target)
                {
                    case "enemy":
                        // Attack
                        //Debug.Log("Attack");
                        //getSelected();
                        selected = selectMan.selectedList;
                        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_attack");
                        playerCommands.AttackCommand(selected, crosshair.targetObject); //
                        minionsounds.primeMinionSounds(1, selected);
                        break;
                    case "neutral":
                        // Goto
                        //Debug.Log("Go Here, Pos:" + crosshair.targetPos);
                        //getSelected();
                        selected = selectMan.selectedList;
                        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_goto");
                        playerCommands.GoToCommand(selected, crosshair.targetPos); //
                        minionsounds.primeMinionSounds(0, selected);
                        break;
                    case "special":
                        // Special
                        //Debug.Log("Activate Ability");
                        //getSelected();
                        selected = selectMan.selectedList;
                        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
                        playerCommands.ActivateCommand(crosshair.targetObject);
                        break;
                    case "splicer":
                        // Splicer
                        //Debug.Log("Splicer A-go-go");
                        //getSelected();
                        selected = selectMan.selectedList;
                        //FMODUnity.RuntimeManager.PlayOneShot("event:/   ");
                        splicerController.activate();
                        //Debug.Log("Command Splicer Working");
                        break;
                    default:
                        Debug.Log("Invalid Command");
                        break;
                }
                singleClick = false;
            }
        }
        /*if (INP_PlayerInput.getButtonDown("AllyAbility") && crosshair.target == "special")
        {
            Debug.Log("Activate Ability");
            selected = selectMan.selectedList;
            FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
            playerCommands.ActivateCommand(crosshair.targetObject);
        }*/

        // keys 1-4
        if (INP_PlayerInput.getButtonDown("ActivateAlly1"))
        {
            allAllies = allyMan.getAllies();
            slowAllies = new List<GameObject>(allAllies[3]);
            if (slowAllies.Count != 0)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
                playerCommands.ActivateCommand(slowAllies);
            }
        }
        if (INP_PlayerInput.getButtonDown("ActivateAlly2"))
        {
            allAllies = allyMan.getAllies();
            turretAllies = new List<GameObject>(allAllies[4]);
            if (turretAllies.Count != 0)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
                playerCommands.ActivateCommand(turretAllies);
            }
        }
        if (INP_PlayerInput.getButtonDown("ActivateAlly3"))
        {
            allAllies = allyMan.getAllies();
            necroAllies = new List<GameObject>(allAllies[5]);
            if (necroAllies.Count != 0)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
                playerCommands.ActivateCommand(necroAllies);
            }
        }
        if (INP_PlayerInput.getButtonDown("ActivateAlly4"))
        {
            allAllies = allyMan.getAllies();
            confusionAllies = new List<GameObject>(allAllies[6]);
            if (confusionAllies.Count != 0)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
                playerCommands.ActivateCommand(confusionAllies);
            }
        }

        // z key
        if (INP_PlayerInput.getButtonDown("GroupUp"))
        {
            List<List<GameObject>> currentGroups = detectGroups();
            List<Vector3> groupPoints = setGroupUpPoints(currentGroups);
            for(int i = 0; i < groupCount; i++)
            {
                playerCommands.GoToCommand(currentGroups[i], groupPoints[i]);
            }
        }
    }

    void alternativeFollowCommand()
    {
        commandPointToHome();
        //Debug.Log("Follow");
        selected = selectMan.selectedList;
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_follow");
        playerCommands.FollowCommand(selected);
        minionsounds.primeMinionSounds(0, selected);
    }

    /* void getSelected()
    {
        if(INP_PlayerInput.getButtonDown("SelectAll"))
        {
            selected = allyMan.getAllAllies;
        }
            if (selected.Count == 0)
                {
                selected = new List<GameObject>(allyMan.followGroup.AllyList);
                } 
    } */


    //{ flowerMinion, mushroomMinion, treeMinion, slowMinion, turretMinion, necroMinion, confusionMinion };
    List<List<GameObject>> detectGroups()
    {
        List<List<GameObject>> currentGroups = new List<List<GameObject>>();
        allAllies = allyMan.getAllies();
        groupCount = 0;
        foreach (List<GameObject> allyGroup in allAllies)
        {
            List<GameObject> tempGroup = new List<GameObject>(allyGroup);
            if(tempGroup.Count != 0)
            {
                currentGroups.Add(tempGroup);
                groupCount += 1;
            }
        }
        return currentGroups;
    }

    List<Vector3> setGroupUpPoints(List<List<GameObject>> currentGroups)
    {
        List<Vector3> points = new List<Vector3>();
        float pointOneOffset = (groupCount - 1) * (180 / 12);
        Vector3 midVector = (crosshair.targetPos - worldCamera.transform.position);
        midVector.Normalize();
        midVector.y = 0;
        for (int i = 0; i < groupCount; i++)
        {
            float offset = pointOneOffset - (i * (180 / 6));
            Vector3 offsetVector = Quaternion.Euler(0, offset, 0) * midVector;
            Vector3 offsetPosition = player.transform.position + (offsetVector * groupUpRadius);
            Vector3 worldPoint = new Vector3(offsetPosition.x, 100, offsetPosition.z);
            worldPoint = UTL_Dungeon.snapPositionToTerrain(worldPoint);
            points.Add(worldPoint);
        }
        /*
        foreach (Vector3 point in points)
        {
            Debug.Log(point);
        }
        */
        return points;
    }

    void moveCommandPoint(Vector3 newPos)
    {
        if (crosshair.target == "neutral")
        { 
            commandObject.transform.position = newPos;
        }
        else if(crosshair.target == "enemy")
        {
            commandObject.transform.position = commandObjectHome;
        }
    }

    void commandPointToHome()
    {
        commandObject.transform.position = commandObjectHome;
    }

    void determineClick()
    {
        //if (!INP_PlayerInput.getButton("SelectMode") && !INP_PlayerInput.getButton("SelectMode2"))
        {
            if (!singleClick)
            {
                singleClick = true;
                moveCommandPoint(crosshair.targetPos);
                doubleClickTimeLimit = Time.time;
            }
            else if ((Time.time - doubleClickTimeLimit) < delay) // if true, was a double click
            {
                singleClick = false;
                commandPointToHome();
                //Debug.Log("Follow");
                selected = selectMan.selectedList;
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_follow");
                playerCommands.FollowCommand(selected);
                minionsounds.primeMinionSounds(0, selected);
            }
        }
    }

}

