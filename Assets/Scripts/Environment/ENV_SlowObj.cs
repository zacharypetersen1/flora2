﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_SlowObj : MonoBehaviour {
    public int DC = 1;
    public float lifetime = 30;
    float lifeTimer;
    float startGrowthTimer = 0f;
    float timer;
    public float distToGrow = 3;
    public bool purity;
    bool growing;
    string[] affectsTag = new string[2] { "Minion", "Player"};
    ParticleSystem[] effects;
    bool alreadyOn = false;

    public GameObject lightningEffect;

    void Awake()
    {
        startGrowthTimer = Random.Range(.2f, 3f);
        timer = startGrowthTimer;
        lifeTimer = lifetime;
        growing = true;
        effects = GetComponentsInChildren<ParticleSystem>();
        transform.localScale = new Vector3(Random.Range(.5f, 1f), 1, Random.Range(.5f, 1f));
    }
	// Use this for initialization
	void Start () {
        SetupLightningEffect();
        
	}

    void SetupLightningEffect()
    {
        if (purity)
        {
            foreach (ParticleSystem effect in effects)
            {
                effect.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (ParticleSystem effect in effects)
            {
                effect.Stop();
            }
        }
    }

    void StartLightningEffect()
    {
        foreach (ParticleSystem effect in effects)
        {
            effect.Play();
        }
    }

    // Update is called once per frame
    void Update () {
        if (lifetime <= 0)
        {
            killYourself();
        }
        else lifetime -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
		if(timer> 0)
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if(growing)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + (distToGrow / startGrowthTimer) * TME_Manager.getDeltaTime(TME_Time.type.environment), transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - (distToGrow / startGrowthTimer) * TME_Manager.getDeltaTime(TME_Time.type.environment), transform.position.z);
            }
        }
        else
        {
            if (!alreadyOn)
            {
                StartLightningEffect();
                alreadyOn = true;
            }
            if (!growing)
            {
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        foreach(string tag in affectsTag)
        {
            if (collider.tag == tag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(DC, 0, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.slow }, transform.position));
                    //effectEntitiy(collider);
                    killYourself();
                }
                
            }
        }
        
    }

    void effectEntitiy(Collider collider)
    {
        GameObject obj = collider.gameObject;
        GameObject newEffect = UTL_GameObject.cloneAsChild(lightningEffect, obj);
        PSMeshRendererUpdater meshUpdater = newEffect.GetComponent<PSMeshRendererUpdater>();
        meshUpdater.MeshObject = obj;
        meshUpdater.UpdateMeshEffect();
        newEffect.AddComponent<EFX_KillInXSeconds>();
    }

    public void killYourself()
    {
        growing = false;
        timer = startGrowthTimer;
    }
}
