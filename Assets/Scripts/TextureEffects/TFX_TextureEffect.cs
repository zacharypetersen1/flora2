﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TFX_TextureEffect {

    public abstract Color alter(float x, float y, Color initial);

}
