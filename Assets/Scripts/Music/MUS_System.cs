﻿// Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MUS_System : MonoBehaviour {

    [FMODUnity.EventRef]
    public string music = "event:/music_1";
    public bool playMusic = true;
    FMOD.Studio.EventInstance musEvent;

    [FMODUnity.EventRef]
    public string ambience = "event:/ambience";
    public bool playAmbience = true;
    FMOD.Studio.EventInstance ambEvent;

	[FMODUnity.EventRef]
	public string surfSound = "event:/twig_surf";
	FMOD.Studio.EventInstance surfEvent;

    //Vector3 zero = new Vector3(0,0,0);

    bool fmodNull = false;

    public static MUS_System singleton;

    // Use this for initialization
    void Start() {
        singleton = this;
        try
        {
            musEvent = FMODUnity.RuntimeManager.CreateInstance(music);
        }
        catch(System.Exception e)
        {
            fmodNull = true;
        }
        if (!fmodNull)
        {
            ambEvent = FMODUnity.RuntimeManager.CreateInstance(ambience);
            surfEvent = FMODUnity.RuntimeManager.CreateInstance(surfSound);

            if (playMusic)
            {
                musEvent.start();
            }
            MusicIdle(1);

            if (playAmbience)
            {
                ambEvent.start();
            }
        }

        if (ambEvent != null)
        {
            if (playAmbience)
            {
                ambEvent.start();
            }
        }
        //FMODUnity.RuntimeManager.PlayOneShot(ambience, zero);
    }

    //
    // Music control
    //
    public void MusicIdle(float dPar)
    {
        if (fmodNull) return;
        musEvent.setParameterValue("Idle", dPar);
    }
    
    public void MusicCorrupt(float dPar)
    {
        if (fmodNull) return;
        musEvent.setParameterValue("Corrupt", dPar);
    }
    
    public void MusicBoost(float dPar)
    {
        if (fmodNull) return;
        musEvent.setParameterValue("Surfing", dPar);
    }

    //
    // Ambient control
    //
    public void ambientCorrupt(float dPar)
    {
        if (fmodNull) return;
        ambEvent.setParameterValue("corrupt", dPar);
    }

    //
    // Surf control
    //
    public void surfSoundStart()
    {
        if (fmodNull) return;
        surfEvent.start ();
	}

	public void surfSoundStop()
    {
        if (fmodNull) return;
        surfEvent.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
	}

	public void surfSoundSpeed(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("speed", dPar);
	}

	public void surfSoundTrue(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("isSurfing", dPar);
	}

    public void surfSoundCorrupt(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("corrupt", dPar);
    }
}
