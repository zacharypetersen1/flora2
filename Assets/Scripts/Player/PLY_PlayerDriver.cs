﻿// Zach

using UnityEngine;
using System.Collections;

public class PLY_PlayerDriver : MonoBehaviour {

    public string obj_name = "player";          // for loading JSON

    public float normalSpeed = 1;               // speed at which player moves with no altering effects
    public float normalRotVelocity = 2;

    public float treeRootSpeed = 4;             // speed when using treeRoot ability

    public float surfingSpeed = 1.8f;           // stats for Surfing
    public float surfingRotVelMax = 0.3f;
    public float surfingRotVelMin = 0.8f;

    [HideInInspector]
    public float corruptSurfingSpeed = 1.5f;

    [HideInInspector]
    public bool sensingDirtWall = false;        // records if currently looking at a dirt wall
    [HideInInspector]
    public PLY_MoveState.type activeState;   // curent state of player
    [HideInInspector]
    public PLY_MoveState activeStateReference;          
    [HideInInspector]
    public CHA_Motor motor;

    private ABI_Listener listener;

    int doubleJumped = 3;
    bool _boosted = false;
    public bool boosted
    {
        set { _boosted = value; }
    }

    public float boostSize = 1.2f;

    // FMOD parameters
    bool isIdle = true;
    float idleVal = 1;
    float idleValInterp = 0.005f;


    //
    // Use this for initialization
    //
    void Start () {
        //Test code: initializing values using the JSN_Manager
        //JSN_Manager.getStats(this, obj_name, "1");
        //Debug.Log(normalSpeed);
        //Debug.Log(treeRootSpeed);
        //Debug.Log(mossSpeed);

        // get the motor and set the speed
        motor = gameObject.GetComponent<CHA_Motor>();
        motor.speed = normalSpeed;
        motor.rotationVelocity = normalRotVelocity;

        // get ability listener/executer
        listener = gameObject.GetComponent<ABI_Listener>();

        // set initial state
        setState(PLY_MoveState.type.normal);
    }
	
    //
    // perform jump according to conditions
    //
    void jumpHandler()
    {
        if (!motor.isDuringJumpResetBuffer())
        {
            if (motor.onGround() == CHA_Motor.groundType.terrainGround)
            {
                if (MAP_NatureMap.natureTypeAtPos(gameObject.transform.position) == MAP_NatureData.type.grass)
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_boosted", transform.position);
                    doBoostedJump();
                }
                else
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump", transform.position);
                    motor.jump(.75f);
                    doubleJumped = 1;
                }
            }
            else if (_boosted)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_boosted", transform.position);
                doBoostedJump();
                _boosted = false;
            }
            else if (motor.onGround() == CHA_Motor.groundType.meshGround)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump", transform.position);
                doubleJumped = 1;
                motor.jump(.75f);
            }
            else
            {
                if (doubleJumped < 2)
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_air", transform.position);
                    motor.jump(.75f);
                    doubleJumped = 2;
                }

            }

        }

    }

    void doBoostedJump()
    {
        StumpAnim.activateStumpAnim(transform.position);
        motor.jump(boostSize);
        doubleJumped = 1;
    }


    //
    // Runs once per frame
    //
    void Update()
    {

        //motor.onGround();
        if (motor.onGround() != CHA_Motor.groundType.air)
        {
            doubleJumped = 0;
        }

        if (INP_PlayerInput.getButtonDown("Jump"))
        {
            jumpHandler();
        }



        // check all abilities for input
        listener.checkAllAbilities();

        // run current state's update function
        activeStateReference.StateUpdate();

        // Fade music track for idling
        isIdle = (motor.getVelocity2D() != Vector2.zero) ? false : true;
        idleVal += isIdle ? idleValInterp : -idleValInterp;
        idleVal = Mathf.Clamp(idleVal, 0, 1);
        if (MUS_System.singleton != null) { 
            MUS_System.singleton.MusicIdle(idleVal);
        }

    }



    //
    // factory function, creates and sets the current state of the player
    //
    public void setState(PLY_MoveState.type state)
    {
        // destroy currently active state and replace it
        Destroy(activeStateReference);
        switch (state)
        {
            case PLY_MoveState.type.normal:    activeStateReference = gameObject.AddComponent<PLY_State_Normal>(); break;
            case PLY_MoveState.type.treeroots: activeStateReference = gameObject.AddComponent<PLY_State_TreeRoot>(); break;
            case PLY_MoveState.type.vines: activeStateReference = gameObject.AddComponent<PLY_State_Vines>(); break;
            case PLY_MoveState.type.surf: activeStateReference = gameObject.AddComponent<PLY_StateSurf>(); break;
        }

        // record active state
        activeState = state;

        // must call "StateEnter" function here instead of within constructor
        activeStateReference.StateEnter();
    }





    //
    // Called once per frame at fixed time
    //
    void FixedUpdate () {

        // call update on current active state
        activeStateReference.StateFixedUpdate();
	}
}
