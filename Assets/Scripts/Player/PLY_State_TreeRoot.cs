﻿// Zach

using UnityEngine;

public class PLY_State_TreeRoot : PLY_MoveState {

    float rumbleIntensity = 0.25f;
    int wallCount = 0;                  // number of dirt walls currently colliding with
    int exitPadding = 4;                // frames after exit before returning to normal state
    bool exiting = false;               // true when going through exit padding frames
    CHA_Motor motor;
    PLY_PlayerDriver driver;
    Vector2 movementDir;                // directional vector of movement
    ABI_TreeRootsParticle particle;



    //
    // Runs when entering the state
    //
    public override void StateEnter()
    {
        // get motor and driver
        motor = gameObject.GetComponent<CHA_Motor>();
        driver = gameObject.GetComponent<PLY_PlayerDriver>();

        // remember direction and switch to treeRoot speed
        movementDir = UTL_Math.angleRadToUnitVec(motor.getRotation());
        motor.speed = driver.treeRootSpeed;
        gameObject.GetComponent<CircleCollider2D>().isTrigger = true;

        RBL_RumbleManager.turnRumbleOn("treeRoots", rumbleIntensity);
    }



    //
    // Runs once per frame
    //
    public override void StateUpdate()
    {
        particle = GameObject.Find("TreeRootsParticle").GetComponent<ABI_TreeRootsParticle>();
        particle.setEnabled(true);

        if (exiting)
            exitPadding--;

        if (exitPadding == 0)
        {
            RBL_RumbleManager.turnRumbleOff("treeRoots");
            particle.setEnabled(false);
            motor.speed = driver.normalSpeed;
            gameObject.GetComponent<CircleCollider2D>().isTrigger = false;
            driver.setState(PLY_MoveState.type.normal);
        }
    }



    //
    // Runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        motor.move(movementDir);
    }



    //
    // Runs when detected collision occuring
    //
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "dirtWall")
        {
            wallCount++;
        }
    }



    //
    // Runs when detected collision over
    //
    void OnTriggerExit2D(Collider2D col)
    {
        if(col.tag == "dirtWall")
        {
            wallCount--;

            // check if exited all walls and it's time to reset state to normal
            if(wallCount <= 0)
            {
                exiting = true;
            }
        }
    }
}
