﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBG_MonsterTest : MonoBehaviour {

    Transform targetTransform;
    CHA_Motor motor;
    Vector3 pastPos;
    PTH_Path path;
    Vector2 endPoint;

    GameObject player;

    GameObject[] rock_polygons;
    GameObject[] dirt_polygons;

    // Use this for initialization
    void Start () {
        //targetTransform = GameObject.Find("end").transform;
        motor = gameObject.GetComponent<CHA_Motor>();


        rock_polygons = GameObject.FindGameObjectsWithTag("rockWall");
        dirt_polygons = GameObject.FindGameObjectsWithTag("dirtWall");
        player = GameObject.Find("Player");

        endPoint = generateVector2();
        //endPoint = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));

        //path = PTH_Finder.findPath(transform.position, targetTransform.position);
        path = PTH_Finder.findPath(transform.position, endPoint);
        //pastPos = targetTransform.position;
        pastPos = endPoint;
    }
	
	// Update is called once per frame
	void Update () {
        if (path!= null)
            motor.moveAlongPath(path);
    }

    public void goToRandomPosition()
    {
        if ((UTL_Math.vec3ToVec2(transform.position) - endPoint).magnitude <= 2)
        {
            endPoint = generateVector2();
            //endPoint = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
    }

    public void goToPosition(Vector2 targetPos)
    {
        endPoint = targetPos;
        path = PTH_Finder.findPath(transform.position, endPoint);
    }

    public void goToPlayer()
    {
        endPoint = UTL_Math.vec3ToVec2(player.transform.position);
        path = PTH_Finder.findPath(transform.position, endPoint);
    }

    public bool CanSeePlayer()
    {
        return UTL_Dungeon.checkLOSWalls(transform.position, player.transform.position);
    }

    public bool isNearPlayer(int distance = 5)
    {
        return Vector3.Distance(transform.position, player.transform.position) < distance;
    }

    public void attack()
    {
        print(name + " viciously attacks the player");
    }

    public Vector3 wait()
    {
        return transform.position;
    }

    public Vector2 generateVector2()
    {
        Vector2 point;
        do
        {
            point = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));
        } while (checkPointinCollider(point) == true);
        return point;
    }

    //checks if within sensitivity of destination
    public bool isAtPosition(float sensitivity=0.5f)
    {
        return Vector3.Distance(transform.position, UTL_Math.vec2ToVec3(endPoint, 0)) < sensitivity;
    }

    public bool checkPointinCollider(Vector2 point)
    {
        foreach (GameObject poly in dirt_polygons)
        {
            if (poly.GetComponent<PolygonCollider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        foreach (GameObject poly in rock_polygons)
        {
            if (poly.GetComponent<PolygonCollider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        return false;
    }
}
