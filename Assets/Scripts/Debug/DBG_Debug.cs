﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBG_Debug : MonoBehaviour{



    public bool DebugDrawingEnabled = true;
    private static DBG_Debug singleton;



    //
    // Runs once when object is created
    //
    void Awake()
    {
        singleton = this;
    }



    //
    // checks if global debugging is enabled
    //
    public static bool isEnabled()
    {
        return singleton.DebugDrawingEnabled;
    }

	

    //
    // Draws debug drawing of a single point in world space
    //
    public static void point(Vector3 position)
    {
        if(isEnabled())
            DebugExtension.DebugPoint(position);
    }
    public static void point(Vector2 position)
    {
        if (isEnabled())
            DebugExtension.DebugPoint(UTL_Math.vec2ToVec3(position, 0));
    }
    public static void point(Vector3 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugPoint(position, color);
    }
    public static void point(Vector2 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugPoint(UTL_Math.vec2ToVec3(position, 0), color);
    }



    //
    // draws a debug box
    //
    public static void box(Vector3 position)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(position, Vector3.one));
    }
    public static void box(Vector2 position)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(UTL_Math.vec2ToVec3(position, 0), Vector3.one));
    }
    public static void box(Vector3 position, float size)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(position, Vector3.one * size));
    }
    public static void box(Vector2 position, float size)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(UTL_Math.vec2ToVec3(position, 0), Vector3.one * size));
    }
    public static void box(Vector3 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(position, Vector3.one), color);
    }
    public static void box(Vector2 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(UTL_Math.vec2ToVec3(position, 0), Vector3.one), color);
    }
    public static void box(Vector3 position, float size, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(position, Vector3.one * size), color);
    }
    public static void box(Vector2 position, float size, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugBounds(new Bounds(UTL_Math.vec2ToVec3(position, 0), Vector3.one * size), color);
    }



    //
    // draws a debug circle
    //
    public static void circle(Vector3 position)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(position, Vector3.forward);
    }
    public static void circle(Vector2 position)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(UTL_Math.vec2ToVec3(position, 0), Vector3.forward);
    }
    public static void circle(Vector3 position, float radius)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(position, Vector3.forward, radius);
    }
    public static void circle(Vector2 position, float radius)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(UTL_Math.vec2ToVec3(position, 0), Vector3.forward, radius);
    }
    public static void circle(Vector3 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(position, Vector3.forward, color);
    }
    public static void circle(Vector2 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(UTL_Math.vec2ToVec3(position, 0), Vector3.forward, color);
    }
    public static void circle(Vector3 position, float radius, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(position, Vector3.forward, color, radius);
    }
    public static void circle(Vector2 position, float radius, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugCircle(UTL_Math.vec2ToVec3(position, 0), Vector3.forward, color, radius);
    }



    //
    // draws a debug sphere
    //
    public static void sphere(Vector3 position)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(position);
    }
    public static void sphere(Vector2 position)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(UTL_Math.vec2ToVec3(position, 0));
    }
    public static void sphere(Vector3 position, float radius)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(position, radius);
    }
    public static void sphere(Vector2 position, float radius)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(UTL_Math.vec2ToVec3(position, 0), radius);
    }
    public static void sphere(Vector3 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(position, color);
    }
    public static void sphere(Vector2 position, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(UTL_Math.vec2ToVec3(position, 0), color);
    }
    public static void sphere(Vector3 position, float radius, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(position, color, radius);
    }
    public static void sphere(Vector2 position, float radius, Color color)
    {
        if (isEnabled())
            DebugExtension.DebugWireSphere(UTL_Math.vec2ToVec3(position, 0), color, radius);
    }



    //
    // draws a debug line
    //
    public static void line(Vector3 start, Vector3 end)
    {
        if(isEnabled())
            Debug.DrawLine(start, end);
    }
    public static void line(Vector2 start, Vector2 end)
    {
        if (isEnabled())
            Debug.DrawLine(UTL_Math.vec2ToVec3(start, 0), UTL_Math.vec2ToVec3(end, 0));
    }
    public static void line(Vector3 start, Vector3 end, Color color)
    {
        if (isEnabled())
            Debug.DrawLine(start, end, color);
    }
    public static void line(Vector2 start, Vector2 end, Color color)
    {
        if (isEnabled())
            Debug.DrawLine(UTL_Math.vec2ToVec3(start, 0), UTL_Math.vec2ToVec3(end, 0), color);
    }
}
