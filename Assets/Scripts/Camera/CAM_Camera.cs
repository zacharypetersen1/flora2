﻿// Zach

using UnityEngine;
using System.Collections;

public class CAM_Camera : MonoBehaviour {

    public Vector3 offsetVector = new Vector3(0, -7, -10);      // direction vector from player pos towards camera
    private GameObject player;                                  // reference to player
    private CAM_State activeState;                              // reference to current state in the camera FSM
    public CAM_State.type currentState;                         // type of current state
    public Vector2 directionVector;                             // directional vector that camera is facing
    public static CAM_Camera singleton;
    public float fixedSpeedScalar = 1;
    private CAM_Dolly dolly;

    //
    // Use this for initialization
    //
    void Start () {
        singleton = this;
        player = GameObject.Find("Player");
        dolly = GameObject.Find("CameraDolly").GetComponent<CAM_Dolly>();
        setState(CAM_State.type.free);
        //transform.position = dolly.getCamTransform().position;
        //cameraScript.transform.rotation = Quaternion.Lerp(cameraScript.transform.rotation, targetRotation, slideSpeed);
    }



    //
    // Update is called once per frame
    //

    //testing
    private void LateUpdate()
    {
        // New way of getting Player Input
        // Ability1 == Left Mouse Button
        /* Moved to Purify Ability
        if (INP_PlayerInput.getButtonDown("Ability1"))
        {
            setState(CAM_State.type.targeting);
        }
        if (INP_PlayerInput.getButtonUp("Ability1"))
        {
            setState(CAM_State.type.returning);
            UTL_Targeting.target = null;
        }
        */
        /* Old Way of doing it, works but sub-optimal
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (currentState == CAM_State.type.targeting)
            {
                setState(CAM_State.type.returning);
            }
            else if (currentState != CAM_State.type.locked)
            {
                setState(CAM_State.type.targeting);
            }
        }
        */
    }



    //
    // Runs once per frame
    //
    public void FixedUpdate () {
        if(activeState != null) activeState.FixedUpdate();
        directionVector = UTL_Math.angleRadToUnitVec(transform.eulerAngles.z);
    }



    //
    // Sets new camera target
    //
    public void setActiveTarget(Vector3 position, Quaternion rotation)
    {
        activeState.targetPosition = position;
        activeState.targetRotation = rotation;
        activeState.distance = Vector3.Distance(singleton.transform.position, position) * fixedSpeedScalar;
    }



    //
    // Sets active state of main camera
    //
    public static void setState(CAM_State.type stateType)
    {
        //CAM_Camera cam = GameObject.Find("Camera").GetComponent<CAM_Camera>();
        switch (stateType)
        {
            case CAM_State.type.free:      singleton.activeState = new CAM_Free();
                                           singleton.currentState = CAM_State.type.free; break;
            case CAM_State.type.locked:    singleton.activeState = new CAM_Locked();
                                           singleton.currentState = CAM_State.type.locked; break;
            case CAM_State.type.returning: singleton.activeState = new CAM_Returning();
                                           singleton.currentState = CAM_State.type.returning; break;
            case CAM_State.type.targeting: singleton.activeState = new CAM_LockOn();
                                           singleton.currentState = CAM_State.type.targeting; break;
        }
        //print("Set state to: " + cam.activeState);
    }



    //
    // sets if camera dolly will be influenced by Twig's rotation and movement
    //
    public static void setDollyInfluence(int isInfluenced)
    {
        singleton.dolly.setInfluenced(isInfluenced);
    }



    //
    // Applies camera z rotation (used for transforming input)
    //
    public static Vector2 applyDollyRotZ(Vector2 vec)
    {
        Vector3 temp = new Vector3(vec.x, vec.y, 0);
        temp = Quaternion.Euler(0, 0, singleton.dolly.horizontalRotation) * temp;
        return new Vector2(temp.x, temp.y);
    }



    //
    // gets a mix of the dolly's rotation and also the turn angle- the physical angle of the camera
    //
    public static float getDollyPhysicalRotZ()
    {
        return singleton.dolly.getPhysicalRot();
    }
}
