﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAM_Dolly : MonoBehaviour
{



    CHA_Motor motor;                                        // player's transform
    Transform targetTransform;
    public Vector3 directionVec = new Vector3(-8, 13, 0);  // will become unit vector indicating cam direction
    public float targetMagnitude = 8;
    public float curMagnitude = 8;                                 // magnitude, or distance of camera away
    public float magScrollSpeed = 16;
    public float magnitudeUpperClamp = 20;
    public float rotationLimit = 10;
    public float magnitudeLowerClamp = 2;
    public float minVerticalClamp = 85; // Clamps camera when looking up
    public float maxVerticalClamp = 315; // Clamps camera when looking down

    //bool influenceCamRotation = false;                       // determines behavior of camera dolly
    public float horizontalRotation = 0;
    public float verticleRotation = 0;
    float targetRot = 0;

    float influencedDeltaRotTHold = 0;                                   
    public float baseInfluencedSpeed = 2.5f;
    public float influencedTurnRange = 45;
    public float targetTurnAngle = 0;
    private float curTurnAngle = 0;
    public float camHorizontalSpeed = 2;
    public float camVerticleSpeed = 2;
    public float maxRotationSpeed = 3f;

    public static bool locked = false;

    enum influenceModes : int { uninfluenced = 0, influenced = 1, vines = 2 }
    int influenceMode = 0;

    //
    // Use this for initialization
    //
    void Start()
    {
        motor = GameObject.Find("Player").GetComponent<CHA_Motor>();
        //directionVec = directionVec.normalized;
        targetTransform = new GameObject("CameraDollyTarget").transform;
        setInfluenced(0);
    }

    bool influencedCamRotation()
    {
        return (influenceMode == (int)influenceModes.influenced);
    }

    //
    // Update is called once per frame
    //
    void FixedUpdate()
    {

        // toggle camera mode if player presses button
        /*if (INP_PlayerInput.getButtonDown("ToggleCamera"))
        {
            influenceCamRotation = !influenceCamRotation;
        }*/

        //if dolly is in influenced mode, interpolate current turn angle toward target turn angle
        targetTurnAngle = influencedCamRotation() ? influencedTurnRange * -INP_PlayerInput.get2DAxis("Camera", false).x : 0;
        curTurnAngle = Mathf.Lerp(curTurnAngle, targetTurnAngle, 0.035f);

        // interpolate magnitude toward target magnitude
        curMagnitude = Mathf.Lerp(curMagnitude, targetMagnitude, 0.08f);

        // move dolly toward player
        transform.position = motor.transform.position;//Vector3.Slerp(transform.position, motor.transform.position, 0.5f);

        // update dolly's rotation
        if (!locked)
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
            transform.Rotate(new Vector3(0, -horizontalRotation /*- curTurnAngle*/, 0));
            transform.Rotate(new Vector3(-verticleRotation, 0, 0), Space.Self);
        }
    }



    //
    // sets if the dolly rotation will be influenced by Twig's rotation and movement
    //
    public void setInfluenced(int influenceMode)
    {
        /*this.influenceMode = influenceMode;
        switch (influenceMode)
        {
            case (int)influenceModes.influenced:
                targetMagnitude = magnitudeLowerClamp;
                break;

            case (int)influenceModes.uninfluenced:
                targetMagnitude = magnitudeLowerClamp + 4;
                break;

            case (int)influenceModes.vines:
                targetMagnitude = magnitudeLowerClamp;
                break;
        }*/
    }



    //
    // gets the camera target location based on the current state of the dolly
    //
    public Transform getCamTransform()
    {
        // get [0, 1] scalar for camera angle and apply sin & exponential curve to shape it
        float rotationScalar = Mathf.Min(1f, (curMagnitude - magnitudeLowerClamp) / (rotationLimit - magnitudeLowerClamp));
        rotationScalar = Mathf.Sin((Mathf.PI / 2) * rotationScalar);
        rotationScalar = 1 - rotationScalar;
        rotationScalar = Mathf.Pow(rotationScalar, 2);

        // move the target transform to the target camera positon
        targetTransform.position = transform.position;
        targetTransform.rotation = transform.rotation;
        targetTransform.Translate(directionVec * curMagnitude);
        targetTransform.Rotate(new Vector3(30, 0, 0), Space.Self);
        //targetTransform.Rotate(new Vector3(30 + 40 * rotationScalar, 0, 180), Space.Self);

        return targetTransform;
    }



    //
    // gets a mix of the dolly's rotation and also the turn angle- the physical angle of the camera
    //
    public float getPhysicalRot()
    {
        return horizontalRotation + curTurnAngle;
    }



    //
    // allows dolly to adjust by listening to input
    //
    public void adjust()
    {
        switch (influenceMode)
        {

            // Runs when camera is in "locked mode" (i.e. movement controls camera)
            case (int)influenceModes.influenced:
                updateInfluenced();
                break;

            // Runs when camera is in "free mode" (i.e. player controls camera)
            case (int)influenceModes.uninfluenced:
                updateNonInfluenced();
                break;

            // Runs when camera is in "locked mode" (i.e. movement controls camera)
            case (int)influenceModes.vines:
                updateVines();
                break;
        }
    }



    //
    // updates the camera dolly as if it is locked into forward facing position
    //
    void updateInfluenced()
    {
        // listen for input for the camera
        Vector2 camAdjust = INP_PlayerInput.get2DAxis("Camera", false);

        // immediatly lock on to target position if near enough
        float motorRot = motor.getRotation() * Mathf.Rad2Deg;
        if (UTL_Math.getDeltaRotDeg(motorRot, horizontalRotation) > influencedDeltaRotTHold)
        {
            targetRot = motorRot;
        }

        // get rotation speed for this frame as a function of player's velocity and size of rotation to target angle
        float velScalar = motor.getVelocityMagnitude();

        float rotScalar = UTL_Math.getDeltaRotDeg(horizontalRotation, targetRot) / 100;
        rotScalar = rotScalar * rotScalar * rotScalar;

        float rotationSpeed = baseInfluencedSpeed * velScalar * rotScalar;

        // clamp rotation speed
        rotationSpeed = Mathf.Clamp(rotationSpeed, 0, maxRotationSpeed);

        // calculate new rotation based on target rotation and rotation speed
        horizontalRotation = UTL_Math.calcRotationDeg(horizontalRotation, targetRot, rotationSpeed);
    }



    //
    // Updates the camera dolly as if it is in "free" mode
    //
    void updateNonInfluenced()
    {
        // listen for input for the camera
        Vector2 camAdjust = INP_PlayerInput.get2DAxis("Camera", false) * Time.deltaTime;

        // adjust the camera dolly's "boom arm" reach distance
        //targetMagnitude = Mathf.Clamp(targetMagnitude + camAdjust.y * -0.4f, magnitudeLowerClamp, magnitudeUpperClamp);
        targetMagnitude = Mathf.Clamp(targetMagnitude + -Input.GetAxis("Mouse ScrollWheel") * magScrollSpeed, 3, 20);
        directionVec.y = UTL_Math.mapToNewRange(curMagnitude, 3, 20, 2.1f, 1.5f);

        // calculate new rotation
        if (!locked)
        {
            horizontalRotation = UTL_Math.calcRotationDeg(horizontalRotation, camAdjust.x * -camHorizontalSpeed);
            verticleRotation = UTL_Math.calcRotationDeg(verticleRotation, camAdjust.y * camVerticleSpeed);
            clampVertical();
        }
    }

    // Clamp camera to prevent it from going upside down
    public void clampVertical()
    {
        if (verticleRotation > minVerticalClamp && verticleRotation < maxVerticalClamp)
        {
            if (Mathf.Abs(maxVerticalClamp - minVerticalClamp) / 2 > verticleRotation) verticleRotation = minVerticalClamp;
            else verticleRotation = maxVerticalClamp;
        }
    }

    //
    //
    //
    void updateVines()
    {
        // don't actually do anything yet you fuck

    }
}