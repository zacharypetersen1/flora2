﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCM_CineCam : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.N))
        {
            setWorldSpace("Player", "front");
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            setWorldSpace("Player", "side");
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            setScreenSpace("bot_left");
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            setScreenSpace("bot_right");
        }

    }

    //
    // Dictionary for CineCam camera "angle/shot"
    // Key is string name for shot, value is an array of Vector3's, Position then rotation
    //
    public Dictionary<string, Vector3[]> cameraShots = new Dictionary<string, Vector3[]>()
    {
        {
            "front", new Vector3[]
            {
                new Vector3(1.32f, 0.289608f, -2.9f), new Vector3(10.7f, -72.54f, 91.3f)
            }
        },
        
        {
            "side", new Vector3[]
            {
                new Vector3( 0.6800003f, 0.6f, -2.9f), new Vector3(56.294f, -61.203f, 120.171f)
            }
        }
    };

    //
    // Dictionary with camera window positions for the screen space
    // Key is a string name for the placement, value is an Array of floats [x , y, width, height]
    //
    public Dictionary<string, float[]> screenPositions = new Dictionary<string, float[]>()
    {
        {
            "bot_left", new float[]
            {
                0f, 0f, .3f, .3f
            }
        },

        {
            "bot_right", new float[]
            {
                .7f, 0f, .3f, .3f
            }
        }
    };

    //
    // Function to set World coordinate offset for the CineCam in relation to the chosen character
    // Takes an object name and shot name
    //
    public void setWorldSpace(string objName, string shotAngle)
    {
        GameObject character = GameObject.Find(objName);
        transform.SetParent(character.transform);
        transform.localPosition = cameraShots[shotAngle][0];
        transform.localEulerAngles = cameraShots[shotAngle][1];
    }

    //
    // Function to set the Screen coordinates for the CineCam window
    // Takes a string
    //
    public void setScreenSpace(string position)
    {
        GameObject CineCam = GameObject.Find("CineCam");
        float[] pos = screenPositions[position];
        CineCam.GetComponent<Camera>().rect = new Rect(pos[0], pos[1], pos[2], pos[3]);
    }

}
