﻿// Andrei

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEL_SelectionModeCamera : MonoBehaviour {

    CAM_Dolly dolly;

    public float minHorizSpeed = 0;
    public float maxHorizSpeed = 4;
    public float minVertSpeed = 0;
    public float maxVertSpeed = 4;
    public int horizontalMargin = 10; // Percent
    public int verticalMargin = 10;   // Percent

    float leftMargin;                 // Also the size of the horizontal margins
    float rightMargin;
    float botMargin;                  // Also the size of the vertical margins
    float topMargin;

    float mouseX;
    float mouseY;

    // Use this for initialization
    void Start () {
        dolly = GameObject.Find("CameraDolly").GetComponent<CAM_Dolly>();
        leftMargin = Screen.width * (horizontalMargin / 100f); 
        rightMargin = Screen.width * (1 - horizontalMargin / 100f);
        botMargin = Screen.height * (verticalMargin / 100f);
        topMargin = Screen.height * (1 - verticalMargin / 100f);
    }
	
	// Update is called once per frame
	void Update () {
        if (INP_PlayerInput.getButton("SelectMode"))
        {
            mouseX = INP_MouseCursor.position.x;
            mouseY = INP_MouseCursor.position.y;
            setHorizRot();
            setVertRot();
            clampRot();
        }
	}

    void setHorizRot()
    {
        float intensity = 0;
        // Left
        if (mouseX < leftMargin)
        {
            intensity = (leftMargin - mouseX) / leftMargin;
            dolly.horizontalRotation += calcHorizSpeed(intensity);
        }
        // Right
        else if (mouseX > rightMargin)
        {
            intensity = (mouseX - rightMargin) / leftMargin;
            dolly.horizontalRotation -= calcHorizSpeed(intensity);
        }
    }

    void setVertRot()
    {
        float intensity = 0; // Stays between [0, 1]
        // Bottom
        if (mouseY < botMargin)
        {
            intensity = (botMargin - mouseY) / botMargin;
            dolly.verticleRotation -= calcVertSpeed(intensity);
        }
        // Top
        else if (mouseY > topMargin)
        {
            intensity = (mouseY - topMargin) / botMargin;
            dolly.verticleRotation += calcVertSpeed(intensity);
        }
    }

    // Calculate how much to vertically rotate the camera, clamped by minVertSpeed and maxVertSpeed
    float calcVertSpeed(float intensity)
    {
        return (minVertSpeed + maxVertSpeed * intensity);
    }



    // Calculate how much to horizontally rotate the camera, clamped by minHorizSpeed and maxHorizSpeed
    float calcHorizSpeed(float intensity)
    {
        return (minHorizSpeed + maxHorizSpeed * intensity);
    }

    // Clamp rotation
    void clampRot()
    {
        dolly.horizontalRotation %= 360;
        dolly.verticleRotation %= 360;
        if (dolly.horizontalRotation < 0) dolly.horizontalRotation += 360;
        if (dolly.verticleRotation < 0) dolly.verticleRotation += 360;
        dolly.clampVertical();
    }
}
