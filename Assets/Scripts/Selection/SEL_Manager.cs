﻿// Andrei & Sterling
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SEL_Manager : MonoBehaviour {
    SelectionManager selectionManager;
    public List<GameObject> selectedList;
    public GameObject crosshair;

    // Use this for initialization
    void Start () {
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        selectedList = new List<GameObject>();
        Cursor.visible = false;
        crosshair = GameObject.Find("Crosshair");
    }

    // Update is called once per frame
    void Update() {
        selectedList = selectionManager.GetSelectedObjectsAsList();

        if (INP_PlayerInput.getButton("SelectMode"))
        {
            crosshair.GetComponent<MeshRenderer>().enabled = false;
            //CAM_Dolly.locked = true;
            CAM_Free.frozen = true;
            SelectionManager.selectMode = true;
            TME_Manager.setTimeScalar(TME_Time.type.game, 1);
        }
        else
        {
            crosshair.GetComponent<MeshRenderer>().enabled = true;
            //CAM_Dolly.locked = false;
            CAM_Free.frozen = false;
            SelectionManager.selectMode = false;
            TME_Manager.setTimeScalar(TME_Time.type.game, 1);
        }

        if (INP_PlayerInput.getButtonDown("SelectAll"))
        {
            selectionManager.toggleSelectAll();
        }
    }

    // Returns pair of dictionaries, one containing amount of selected minions stored by type, and one containing the total amount of minions stored by type
    public Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>> getSelected()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        Dictionary<ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in selectedList)
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }
        return new Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>>(selectedCount, totalCount);
        
    }

    public Dictionary<ENT_DungeonEntity.monsterTypes, int> getHovered()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        // Count selected minions
        Dictionary <ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in GetComponent<SelectionManager>().GetHoveredObjects())
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }

        return selectedCount;

    }

    // My failed / unfinished attempt to use generics

    //Dictionary<ENT_DungeonEntity.monsterTypes, int> countMinions<T>(List<T> minions)
    //{
    //    Dictionary<ENT_DungeonEntity.monsterTypes, int> count = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
    //    foreach (T obj in minions)
    //    {
    //        var type = obj.GetComponent<ENT_Body>().entity_type;
    //        if (count.ContainsKey(type))
    //        {
    //            count[type] += 1;
    //        }
    //        else count.Add(type, 1);
    //    }
    //    return count;
    //}
}
