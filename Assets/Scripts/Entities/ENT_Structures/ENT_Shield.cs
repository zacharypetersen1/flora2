﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ENT_Shield : ENT_Structure {
    public ENT_DungeonEntity entity_to_shield;
    bool applied_shield;
    public Material[] shield_materials = new Material[2];
    GameObject shield_effect;
    //Uses degrees
    public float beamAngle;
    public int numPulses = 6;
    private List<GameObject> pulses = new List<GameObject>();
    private float numSteps;
    private float pulseDelay; //in seconds

    private float count = 0; //tracks update cycles


    // Use this for initialization
    public override void Start () {
        base.Start();
        State = "active";
        applied_shield = false;
        shield_entity();
        for(int i = 0; i < numPulses; i++)
        {
            GameObject pulse = UTL_GameObject.cloneAtLocation("shieldbeam", transform.position);
            ENT_ShieldSkyPulse beamPulse = pulse.GetComponent<ENT_ShieldSkyPulse>();
            beamPulse.origin = this;
            beamPulse.target = entity_to_shield;
            pulses.Add(pulse);
        }
        numSteps = Vector2.Distance(new Vector2(gameObject.transform.position.x, gameObject.transform.position.z),
            new Vector2(entity_to_shield.transform.position.x, entity_to_shield.transform.position.z));
        pulseDelay = numSteps / numPulses;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        update_shield_entity();
        
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            /*
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.02f, gameObject.transform.position.z);
            timer += 0.02f;
            if (timer > 6)
            {
                Destroy(gameObject);
            }
            */
            return;
        }
        else if (count > 0 && count/pulseDelay < numPulses)
        {
            pulses[Mathf.FloorToInt(count / pulseDelay)].GetComponent<ENT_ShieldSkyPulse>().launch();
        }
        if (count == 0)
        {
            pulseDelay *= Time.fixedDeltaTime;
        }
        //Increment count
        count += Time.fixedDeltaTime;
    }

    public void update_shield_entity()
    {
        if (State == "active")
        {
            try
            {
                if (entity_to_shield.shields >= 2)
                {
                    shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                }
                else if (entity_to_shield.shields == 1)
                {
                    shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public void shield_entity()
    {
        if (!applied_shield && entity_to_shield != null)
        {
            if (entity_to_shield.shields <= 0)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                shield_effect = UTL_GameObject.cloneAtLocation("Environment Effects/Shield/Shield", entity_to_shield.transform.position);
                shield_effect.transform.SetParent(entity_to_shield.transform);

                try
                {
                    if (entity_to_shield.shields >= 2)
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                    }
                    else
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }               
            }
            else if (entity_to_shield.shields >= 1)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                try
                {
                    foreach (Transform child in entity_to_shield.transform)
                    {
                        if (child.gameObject.tag == "shield")
                        {
                            shield_effect = child.gameObject;
                            break;
                        }
                    }
                    if (entity_to_shield.shields >= 2)
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                    }
                    else
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("no ParticleSystem Renderer");
                }
            }
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        entity_to_shield.shields -= 1;
        applied_shield = false;
        if (entity_to_shield.shields <= 0)
        {
            Destroy(shield_effect);
        }
        State = "cleansed";
        rend.material = ghostMat;
    }
}
