﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Remnant : MonoBehaviour {
    public ENT_DungeonEntity.monsterTypes entity_type;
    float remnantTimer = 500F;
    List<List<GameObject>> ListsImIn = new List<List<GameObject>>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        remnantTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
		if(remnantTimer <= 0)
        {
            Destroy(gameObject);
        }
	}

    public void revive(bool asAlly)
    {
        GameObject resurrectEffect;
        if (asAlly)
        {
            resurrectEffect = UTL_GameObject.cloneAtLocation("Particles/ally_Resurrect", transform.position);
        }
        else
        {
            resurrectEffect = UTL_GameObject.cloneAtLocation("Particles/enemy_Resurrect", transform.position);
        }
             
        ENT_ResurrectParticleEffect effect = resurrectEffect.GetComponent<ENT_ResurrectParticleEffect>();
        Quaternion rot = resurrectEffect.transform.rotation;
        rot.eulerAngles = new Vector3(90, 0, 0);
        resurrectEffect.transform.rotation = rot;
        effect.entity_type = entity_type;
        effect.asAlly = asAlly;
    }

    public void addToList(List<GameObject> newList)
    {
        if (!ListsImIn.Contains(newList))
        {
            newList.Add(gameObject);
            ListsImIn.Add(newList);
        }
    }

    public void removeFromList(List<GameObject> newList)
    {
        if (ListsImIn.Contains(newList))
        {
            newList.Remove(gameObject);
            ListsImIn.Remove(newList);
        }
    }

    void OnDestroy()
    {
        List<List<GameObject>> removeList = new List<List<GameObject>>(ListsImIn);
        foreach (var toRemove in removeList)
        {
            removeFromList(toRemove);
        }
    }
}
