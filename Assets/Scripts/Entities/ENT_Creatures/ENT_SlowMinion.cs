﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SlowMinion : ENT_Body {

    ENT_SlowAbility abi;

    float slowAbilityTimer = 3f;
    public float SLOW_DURATION = 3f;

    public override void Awake()
    {
        base.Awake();
        abi = GetComponent<ENT_SlowAbility>();
    }

    public override void Update()
    {
        base.Update();
        slowAbilityTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (slowAbilityTimer <= 0)
        {
            if (isAbilityActive) {
                EndSlow();
            }
        }
    }

    public override void purify_self()
    {
        ENT_SlowAbility ability = GetComponent<ENT_SlowAbility>();
        if (isAlly)
        {
            ability.endAllyAbility();
        }
        else
        {
            ability.endEnemyAbility();
        }
        base.purify_self();
    }

    public void Slow()
    {
        slowAbilityTimer = SLOW_DURATION;
        if (purity)
        {
            abi.activateAllyAbility();
        }
        else
        {
            abi.activateEnemyAbility();
        }
    }

    public void EndSlow()
    {
        if (purity)
        {
            abi.endAllyAbility();
        }
        else
        {
            abi.endEnemyAbility();
        }
    }
}
