﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_NecroMinion : ENT_Body {
    ENT_NecroAbility abi;

    private ABI_RemnantDetector remnantDetector;
    private List<GameObject> removeList = new List<GameObject>();
    public float allyResurrectDistance = 20F;
    public float enemyResurrectDistance = 15F;

    public float necroAbilityTimer = 3f;
    public float NECRO_DURATION = 3f;

    public override void Start()
    {
        base.Start();
        remnantDetector = GetComponentInChildren<ABI_RemnantDetector>();
    }

    public override void Awake()
    {
        base.Awake();
        abi = GetComponent<ENT_NecroAbility>();
    }

    public override void Update()
    {
        base.Update();
        necroAbilityTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (CurrentTree == BehaviorTree.AllyActivateTree)
        {
            if (necroAbilityTimer <= 0)
            {
                CurrentTree = BehaviorTree.AllyActivateTree;
            }
        }
        if (necroAbilityTimer <= 0)
        {
            if (purity)
            {
                abi.endAllyAbility();
            }
            else
            {
                abi.endEnemyAbility();
            }
        }
    }

    public override void purify_self()
    {
        if (isAlly)
        {
            abi.endAllyAbility();
        }
        else
        {
            abi.endEnemyAbility();
        }
        base.purify_self(); 
    }


    public bool allyResurrect()
    {
        necroAbilityTimer = NECRO_DURATION;
        List<GameObject> remnants = remnantDetector.Remnants;
        if (remnants == null) return false;
        if (remnants.Count == 0) return false;
        removeList.Clear();
        foreach(GameObject remnant in remnants)
        {
            if (remnant == null)
            {
                removeList.Add(remnant);
                continue;
            }
            remnant.GetComponent<ENT_Remnant>().revive(Purity);
            removeList.Add(remnant);
            Destroy(remnant);
        }
        foreach (GameObject dead in removeList)
        {
            remnants.Remove(dead);
        }
        return true;
    }

    public bool remnantsVisible()
    {
        return remnantDetector.Remnants.Count > 0;
    }

    public bool remnantInRange()
    {
        return Vector3.Distance(remnantDetector.getNearestRemnant().transform.position, transform.position) <= enemyResurrectDistance;
    }

    public void goToNearestRemnant()
    {
        if (remnantsVisible())
        {
            goToPosition(remnantDetector.getNearestRemnant().transform.position);
        }
    }

    public bool enemyResurrect()
    {
        necroAbilityTimer = NECRO_DURATION;
        List<GameObject> remnants = remnantDetector.Remnants;
        if (remnants == null) return false;
        if (remnants.Count == 0) return false;
        GameObject remnant = remnantDetector.getNearestRemnant();
        if(remnant == null)
        {
            print("remnant is null");
            return false;
        }
        remnant.GetComponent<ENT_Remnant>().revive(Purity);
        remnantDetector.Remnants.Remove(remnant);
        Destroy(remnant);
        return true;
    }
}
