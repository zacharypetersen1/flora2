﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_NecroAbility : ENT_MinionAbility
{
    public GameObject enemyEffect;
    public GameObject allyEffect;

    private ParticleSystem[] allyPS;
    private ParticleSystem[] enemyPS;

    public override void Awake()
    {
        base.Awake();

        setupParticleSystem();
        endAllyAbility();
        endEnemyAbility();
        
    }

    public void setupParticleSystem()
    {
        //get ally particlesystems
        allyEffect = UTL_GameObject.cloneAsChild(allyEffect, gameObject);
        PSMeshRendererUpdater psMeshScript = allyEffect.GetComponent<PSMeshRendererUpdater>();
        psMeshScript.MeshObject = gameObject;
        psMeshScript.UpdateMeshEffect();

        allyPS = allyEffect.GetComponentsInChildren<ParticleSystem>();
        foreach (var ps in allyPS)
        {
            //print("turning off an ally particle system");
            ps.Stop();
        }


        //get enemy particlesystems
        enemyEffect = UTL_GameObject.cloneAsChild(enemyEffect, gameObject);
        PSMeshRendererUpdater psMeshScript_1 = enemyEffect.GetComponent<PSMeshRendererUpdater>();
        psMeshScript_1.MeshObject = gameObject;
        psMeshScript_1.UpdateMeshEffect();

        enemyPS = enemyEffect.GetComponentsInChildren<ParticleSystem>();
        foreach (var ps in enemyPS)
        {
            //print("turning off an enemy particle system");
            ps.Stop();
        }
    }

    protected override void _allyAbility()
    {
        base._allyAbility();
        //Debug.Log("start");
        bool wasSuccessful = gameObject.GetComponent<ENT_NecroMinion>().allyResurrect();
        if (wasSuccessful)
        {
            foreach (var ps in allyPS)
            {

                ps.Play();
            }
        }else
        {
            allyCoolDownTimer = 0;
        }
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        //Debug.Log("start enemy ability");
        bool wasSuccessful = gameObject.GetComponent<ENT_NecroMinion>().enemyResurrect();
        if (wasSuccessful)
        {
            foreach (var ps in enemyPS)
            {

                ps.Play();
            }
        }else
        {
            enemyCoolDownTimer = 0;
        }

    }

    public override void endAllyAbility()
    {
        base.endAllyAbility();
        foreach (var ps in allyPS)
        {
            ps.Stop();
        }
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        foreach (var ps in enemyPS)
        {
            ps.Stop();
        }
    }
}
