﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ConfusionAbility : ENT_MinionAbility
{

    private GameObject confusionAbility;

    protected override void _allyAbility()
    {
        base._allyAbility();
        //print("allyAbility entered");
        confusionAbility = UTL_GameObject.cloneAsChild("Abilities/MinionAttacks/ConfusionAbility/confusionAbility", gameObject);
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purity = true;
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        confusionAbility = UTL_GameObject.cloneAsChild("Abilities/MinionAttacks/ConfusionAbility/enemyConfusionAbility", gameObject);
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purity = false;
    }

    public override void endAllyAbility()
    {
        //print("endAllyAbility entered");
        base.endAllyAbility();
        //Debug.Log(gameObject.GetComponent<ENT_Body>().IsAbilityActive);
        //Destroy(slowAbility);
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        //Destroy(slowAbility);
    }
}
