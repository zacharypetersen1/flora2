﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SlowAbility : ENT_MinionAbility {

    private GameObject slowAbility;

    protected override void _allyAbility()
    {
        base._allyAbility();
        //print("allyAbility entered");
        slowAbility = UTL_GameObject.cloneAsChild("Abilities/MinionAttacks/SlowAbility/SlowAbility", gameObject);
        slowAbility.GetComponent<ABI_SlowAbility>().Purity = true;
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        slowAbility = UTL_GameObject.cloneAsChild("Abilities/MinionAttacks/SlowAbility/SlowAbility", gameObject);
        slowAbility.GetComponent<ABI_SlowAbility>().Purity = false;
    }

    public override void endAllyAbility()
    {
        //print("endAllyAbility entered");
        base.endAllyAbility();
        //Debug.Log(gameObject.GetComponent<ENT_Body>().IsAbilityActive);
        //Destroy(slowAbility);
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        //Destroy(slowAbility);
    }

    void OnDestroy()
    {
        if (slowAbility != null) Destroy(slowAbility);
    }
}
