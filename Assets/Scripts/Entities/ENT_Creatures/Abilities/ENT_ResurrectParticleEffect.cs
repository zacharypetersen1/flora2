﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ResurrectParticleEffect : MonoBehaviour {

    float resurrectTimer = 2f;
    [HideInInspector]
    public ENT_DungeonEntity.monsterTypes entity_type;
    [HideInInspector]
    public bool asAlly;

	// Use this for initialization
	void Start () {
        resurrectTimer = 3f;
    }
	
	// Update is called once per frame
	void Update () {
		if (resurrectTimer <= 0)
        {
            GameObject newMinion = UTL_Dungeon.spawnMinion(entity_type, transform.position, asAlly);
            newMinion.GetComponent<ENT_Body>().isGhost = true;
            foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
            {
                ps.Stop();
            }
            resurrectTimer = 100f;
            Destroy(gameObject, 2);
        }
        else
        {
            resurrectTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        }
	}
}
