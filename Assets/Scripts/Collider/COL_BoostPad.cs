﻿// Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COL_BoostPad : MonoBehaviour {

    public float customBoostSize = 1.2f;

    PLY_PlayerDriver player;
	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player").GetComponent<PLY_PlayerDriver>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //
    // check for boost pad
    //
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            //Debug.Log("Boosted");
            player.boosted = true;
        }
        player.boostSize = customBoostSize;
    }

    public void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            player.boosted = false;
        }
        player.boostSize = 1.2f;
    }
}
