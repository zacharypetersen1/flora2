﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Gathering/pickupCrystal")]
    [Help("Moves the monster towards the nearest Crystal")]
    public class pickupCrystal : GOAction
    {
        public override void OnStart()
        {
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.pickupCrystal();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}