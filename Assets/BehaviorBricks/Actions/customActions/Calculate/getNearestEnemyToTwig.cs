﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Calculate/getNearestEnemyToTwig")]
    [Help("gets the enemy nearest to Twig and stores it in outparam nearestEnemyToTwig")]
    public class getNearestEnemyToTwig : GOAction
    {

        [OutParam("nearestEnemyToTwig")]
        [Help("closest enemy to twig")]
        public GameObject nearestEnemyToTwig;

        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                nearestEnemyToTwig = body.allyManager.getNearestEnemyToTwig();
                //Debug.Log("waitPosition: " + waitPosition);

                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}