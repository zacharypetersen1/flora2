﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveNearPlayer")]
    [Help("Gets the component of a given type if the game object has one attached, null if it doesn't")]
    public class MoveNearPlayer : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("move");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                Vector3 vec = (gameObject.transform.position - monsterBody.Player.transform.position).normalized;
                Vector3 finalPos = vec * (monsterBody.myFollowDistance);
                finalPos = monsterBody.Player.transform.position + finalPos;
                if (!monsterBody.isAtPosition(finalPos) && Vector3.Distance(finalPos, monsterBody.Player.transform.position) < Vector3.Distance(gameObject.transform.position, monsterBody.Player.transform.position))
                {
                    monsterBody.goToPosition(finalPos);
                }
                else
                {
                    monsterBody.shouldRotate = true;
                    monsterBody.lookTowardsVec = monsterBody.Player.transform.position;
                    monsterBody.dontMove();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.goToPlayer();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}