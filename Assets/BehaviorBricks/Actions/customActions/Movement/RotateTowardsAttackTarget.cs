﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/RotateTowardsAttackTarget")]
    [Help("Tells the character to rotate towards their attackTarget")]
    public class RotateTowardsAttackTarget : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            body.shouldRotate = true;
            body.lookTowardsVec = body.AttackTarget.transform.position;
            //body.rotate(true, 5F);
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
