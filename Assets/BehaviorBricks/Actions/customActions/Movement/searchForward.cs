﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/SearchForward")]
    [Help("Tells the character to move to their forward search position")]
    public class SearchForward : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            body.searchingForward();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
