﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToNearestVisibleEnemy")]
    [Help("Moves the monster towards the nearest enemy position")]
    public class moveToNearestVisibleEnemy : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestVisibleEnemy(gameObject.transform.position, body.sightRange);
                if (nearestEnemy != null)
                {

                    Vector3 vec = nearestEnemy.transform.position - gameObject.transform.position;
                    float mag;
                    //if ranged else melee
                    if (body.attackRange > 5)
                    {
                        mag = vec.magnitude - (body.attackRange / 2);
                    }
                    else
                    {
                        mag = vec.magnitude;
                    }
                    Vector3 finalPos = vec.normalized * mag;
                    //finalPos = body.Player.transform.position + finalPos;
                    finalPos = gameObject.transform.position + finalPos;

                    if (!body.isAtPosition(finalPos))
                        body.goToPosition(finalPos);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}