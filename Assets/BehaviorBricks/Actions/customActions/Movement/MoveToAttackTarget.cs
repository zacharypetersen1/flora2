﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/MoveToAttackTarget")]
    [Help("this ally attacks whatever value is in their AttackTarget")]
    public class MoveToAttackTarget : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                if (monster.AttackTarget != null) monster.goToPosition(monster.AttackTarget.transform.position);
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}