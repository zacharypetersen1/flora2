﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/PatrolPath")]
    [Help("Patrols between the points specified in patrolPoints.")]
    public class PatrolPath : GOAction
    {

        //Note#1: Get the list of points through the monster body or brain not the behavior tree
        //Note#2: Patrol target has been added to monster body class
        
        [InParam("PointsToPatrol")]
        [Help("List of points to patrol between")]
        public List<Vector2> patrolPoints;
        

        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                monster.patrol();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
