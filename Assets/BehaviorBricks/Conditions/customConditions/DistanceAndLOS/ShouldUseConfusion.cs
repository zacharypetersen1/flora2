﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;
using System.Collections.Generic;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/ShouldUseConfusion")]
    [Help("checks if this confusion enemy's ability is ready and if it is in range of at least 3 enemies")]
    public class ShouldUseConfusion : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_ConfusionMinion body = gameObject.GetComponent<ENT_ConfusionMinion>();
                ENT_ConfusionAbility confAbi = gameObject.GetComponent<ENT_ConfusionAbility>();
                if (confAbi.EnemyCoolDownTimer > 0) return false;
                PLY_AllyManager am = body.allyManager;
                List<GameObject> inRange = am.getAllAlliesWithinRange(gameObject.transform.position, 10); //range of confusion ability
                return (inRange.Count >= 1);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}