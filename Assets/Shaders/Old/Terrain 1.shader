﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Flora/Terrain" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MapTex("Map Texture", 2D) = "white" {}
		_GrassTex("Grass Texture", 2D) = "green" {}
		_RockTex("Rock Texture", 2D) = "gray" {}
		_CorruptTex("Corrupt Texture", 2D) = "purple" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_TexScalar("Texture Scalar", Range(1,10)) = 1
		_MapScalar("Map Scalar", Float) = 1
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fogf work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv     : TEXCOORD0;
				float3 wpos   : TEXCOORD1;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _MapTex;
			sampler2D _GrassTex;
			sampler2D _RockTex;
			sampler2D _CorruptTex;

			float4 _MainTex_ST;
			float _TexScalar;
			float _MapScalar;

			v2f vert(appdata v)
			{
				v2f o;
				o.wpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.uv = v.uv;
				o.vertex = UnityObjectToClipPos(v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 mapCol  = tex2D(_MapTex, i.wpos / _MapScalar);
				fixed4 grssCol = tex2D(_GrassTex, i.uv * _TexScalar);
				fixed4 rockCol = tex2D(_RockTex, i.uv * _TexScalar);
				fixed4 crptCol = tex2D(_CorruptTex, i.uv * _TexScalar);
				fixed4 col = mapCol.r * rockCol + mapCol.g * grssCol + mapCol.b * crptCol;

				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
