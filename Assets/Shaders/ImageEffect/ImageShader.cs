﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ImageShader : MonoBehaviour
{
    private Material material;
    public Camera brushCam;
    RenderTexture brushTexture;
    RenderTexture lastFrame;



    //
    // Creates a private material used to the effect
    //
    void Awake()
    {
        brushTexture = new RenderTexture(Screen.width, Screen.height, 0);
        lastFrame = new RenderTexture(Screen.width, Screen.height, 0);
        brushCam.targetTexture = brushTexture;
        //brushCam.targetTexture = new RenderTexture(1024, 1024, 0);
        material = new Material(Shader.Find("Hidden/ImageShader"));
    }


    //
    // Postprocess the image
    //
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        brushCam.Render();
        material.SetTexture("_BrushMask", brushTexture);
        material.SetTexture("_LastFrame", lastFrame);
        Graphics.Blit(source, destination, material);
        Graphics.Blit(destination, lastFrame);
    }
}
